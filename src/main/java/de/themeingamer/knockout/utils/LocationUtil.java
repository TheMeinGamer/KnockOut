package de.themeingamer.knockout.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class LocationUtil {

	public static String fromLocation(Location location){
		return location.getWorld().getName() + ":" + location.getBlockX() + ":" + location.getBlockY() + ":" + 
			   location.getBlockZ() + ":" + location.getYaw() + ":" + location.getPitch();
	}
	
	public static Location fromString(String location){
		
		String[] split = location.split(":");
		prepareWorld(location);
		World w = Bukkit.getWorld(split[0]);
		double x = Double.valueOf(split[1]);
		double y = Double.valueOf(split[2]);
		double z = Double.valueOf(split[3]);
		float yaw = Float.valueOf(split[4]);
		float pitch = Float.valueOf(split[5]);
		
		return new Location(w, x, y, z, yaw, pitch);
	}
	
	private static void prepareWorld(String location) {
		String world = location.split(":")[0];
		World w = Bukkit.getWorld(world);
		
		if(w == null){
			WorldCreator wc = new WorldCreator(world);
			wc.createWorld();
		
			w = Bukkit.getWorld(world);
			w.setThundering(false);
        	w.setStorm(false);
        	w.setTime(6000);
        	w.setAutoSave(false);
        	w.setGameRuleValue("doDaylightCycle", "false");
        	w.setGameRuleValue("doMobSpawning", "false");
		}
	}

}
