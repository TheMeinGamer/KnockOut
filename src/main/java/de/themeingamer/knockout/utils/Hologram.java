package de.themeingamer.knockout.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Creator: TheMeinGamer
 * Date: 28.04.17
 */
public class Hologram {

    private Plugin plugin;
    private final List<Object> lines;

    private static final double ABS = 0.23D;
    private static String path, version;
    private static Class<?> armorStand, worldClass, nmsEntity, craftWorld, packetClass, entityLivingClass, destroyPacketClass, nmsPacket;
    private static Constructor<?> destroyPacketConstructor, armorStandConstructor;

    static {
        path = Bukkit.getServer().getClass().getPackage().getName();
        version = path.substring(path.lastIndexOf(".") + 1, path.length());

        try {
            armorStand = getNMSClass("EntityArmorStand");
            worldClass = getNMSClass("World");
            nmsEntity = getNMSClass("Entity");
            craftWorld = Class.forName("org.bukkit.craftbukkit." + version + ".CraftWorld");
            packetClass = getNMSClass("PacketPlayOutSpawnEntityLiving");
            entityLivingClass = getNMSClass("EntityLiving");
            destroyPacketClass = getNMSClass("PacketPlayOutEntityDestroy");
            nmsPacket = getNMSClass("Packet");

            armorStandConstructor = armorStand.getConstructor(new Class[]{worldClass});
            destroyPacketConstructor = destroyPacketClass.getConstructor(int[].class);

        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException ex) {
            ex.printStackTrace();
        }
    }

    public Hologram(Plugin plugin, Location loc, String... lines) {
        this(plugin, loc, Arrays.asList(lines));
    }

    public Hologram(Plugin plugin, Location location, List<String> lines) {
        this.plugin = plugin;
        this.lines = new ArrayList<>();

        Location displayLoc = location.clone().add(0, (ABS * lines.size()) - 1.97D, 0);
        for (int i = 0; i < lines.size(); i++) {
            Object packet = getSpawnPacket(location.getWorld(), displayLoc.getX(), displayLoc.getY(), displayLoc.getZ(), lines.get(i));
            this.lines.add(packet);
            displayLoc.add(0, -ABS, 0);
        }
    }

    public void showPlayer(Player p) {
        for (int i = 0; i < lines.size(); i++) {
            sendPacket(p, lines.get(i));
        }
    }

    public void showPlayers(Player... players) {
        for (Player all : players) {
            for (int i = 0; i < lines.size(); i++) {
                sendPacket(all, lines.get(i));
            }
        }
    }

    public void showPlayerTemp(Player p, int ticks) {
        showPlayer(p);
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                if (p != null) hidePlayer(p);
            }
        }, ticks);
    }

    public void hidePlayer(Player p) {
        for (int i = 0; i < lines.size(); i++) {
            sendPacket(p, getDestroyPacket(lines.get(i)));
        }
    }

    public void hidePlayers(Player... players) {
        for (Player all : players) {
            for (int i = 0; i < lines.size(); i++) {
                sendPacket(all, getDestroyPacket(lines.get(i)));
            }
        }
    }

    private Object getSpawnPacket(World w, double x, double y, double z, String text) {
        try {
            Object craftWorldObj = craftWorld.cast(w);
            Method getHandleMethod = craftWorldObj.getClass().getMethod("getHandle", new Class<?>[0]);
            Object entityObject = armorStandConstructor.newInstance(new Object[]{getHandleMethod.invoke(craftWorldObj, new Object[0])});
            Method setCustomName = entityObject.getClass().getMethod("setCustomName", new Class<?>[]{String.class});
            setCustomName.invoke(entityObject, new Object[]{text});
            Method setCustomNameVisible = nmsEntity.getMethod("setCustomNameVisible", new Class[]{boolean.class});
            setCustomNameVisible.invoke(entityObject, new Object[]{true});
            Method setGravity = entityObject.getClass().getMethod("setGravity", new Class<?>[]{boolean.class});
            setGravity.invoke(entityObject, new Object[]{false});
            Method setLocation = entityObject.getClass().getMethod("setLocation", new Class<?>[]{double.class, double.class, double.class, float.class, float.class});
            setLocation.invoke(entityObject, new Object[]{x, y, z, 0.0F, 0.0F});
            Method setInvisible = entityObject.getClass().getMethod("setInvisible", new Class<?>[]{boolean.class});
            setInvisible.invoke(entityObject, new Object[]{true});
            Constructor<?> cw = packetClass.getConstructor(new Class<?>[]{entityLivingClass});
            Object packetObject = cw.newInstance(new Object[]{entityObject});
            return packetObject;
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object getDestroyPacket(Object line) {
        try {
            Field field = packetClass.getDeclaredField("a");
            field.setAccessible(true);
            int[] id = new int[]{(int) field.get(line)};

            return destroyPacketConstructor.newInstance(id);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void sendPacket(Player p, Object packet) {
        try {
            Method getHandle = p.getClass().getMethod("getHandle");
            Object entityPlayer = getHandle.invoke(p);
            Object pConnection = entityPlayer.getClass().getField("playerConnection").get(entityPlayer);
            Method sendMethod = pConnection.getClass().getMethod("sendPacket", new Class[]{nmsPacket});
            sendMethod.invoke(pConnection, new Object[]{packet});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Class<?> getNMSClass(String className) {
        String fullName = "net.minecraft.server." + version + "." + className;
        Class<?> clazz = null;
        try {
            clazz = Class.forName(fullName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clazz;
    }

}


