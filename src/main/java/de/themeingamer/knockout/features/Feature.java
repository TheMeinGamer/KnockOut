package de.themeingamer.knockout.features;

import java.util.ArrayList;
import java.util.Arrays;

import de.themeingamer.knockout.language.Language;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class Feature {

	private ItemStack icon;
	private ArrayList<String> description;
	private FeatureManager.FeatureType featuretype;
	
	public Feature(FeatureManager.FeatureType featuretype, ItemStack icon, String... description) {
		this.featuretype = featuretype;
		this.icon = icon;
		this.description = new ArrayList<>();
		this.description.addAll(Arrays.asList(description));
	}

	public FeatureManager.FeatureType getFeaturetype() {
		return featuretype;
	}

	public ItemStack getIcon() {
		return icon;
	}

	// '#NT#' = NoTranslation
	public String getDescription(Player p) {
		String translated = "";
		for(String s : description){
			if(s.startsWith("#NT#")) {
				translated += s.replace("#NT#","");
			}else{
				translated += Language.translate(p, s);
			}
		}
		return translated;
	}




}
