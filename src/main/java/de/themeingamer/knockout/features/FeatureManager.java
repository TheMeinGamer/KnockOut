package de.themeingamer.knockout.features;

import java.util.ArrayList;
import java.util.HashMap;

import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;

import de.themeingamer.knockout.itemfactory.ItemFactory;
import de.themeingamer.knockout.itemfactory.LeatherArmorFactory;
import de.themeingamer.knockout.itemfactory.LeatherArmorFactory.LeatherArmorType;
import de.themeingamer.knockout.itemfactory.DyedFactory;
import de.themeingamer.knockout.itemfactory.DyedFactory.DyeType;
import de.themeingamer.knockout.itemfactory.SkullFactory;
import de.themeingamer.knockout.itemfactory.SkullFactory.SkullType;
import org.bukkit.inventory.ItemStack;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class FeatureManager {

	private HashMap<Integer,Feature> features;
	
	public FeatureManager() {
		features = new HashMap<>();
		loadFeatures();
	}


	private void loadFeatures() {
		features.put(1, new Feature(FeatureType.LEVEL,new DyedFactory(DyeType.DYE).setColor(DyeColor.LIME).build(),FeatureType.LEVEL.getText() , "FEATURE_FEATURELEVEL_BRIGHTGREEN"));
		features.put(2, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_HELMET).setColor(Color.GREEN).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.HELMET.getText()));
		features.put(3, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bAMBIENCE_THUNDER"));
		features.put(4, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_BOOTS).setColor(Color.GREEN).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.BOOTS.getText()));
		features.put(5, new Feature(FeatureType.WEAPON,new ItemFactory(Material.WOOD_HOE).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_WOOD_HOE"));
		features.put(6, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_LEGGINGS).setColor(Color.GREEN).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.LEGGINGS.getText()));
		features.put(7, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bITEM_BREAK"));
		features.put(8, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_CHESTPLATE).setColor(Color.GREEN).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.CHESTPLATE.getText()));
		features.put(9, new Feature(FeatureType.SKULL,KnockOut.getInstance().getPlayerHeadManager().getHead(0),FeatureType.SKULL.getText()));

		features.put(10, new Feature(FeatureType.LEVEL,new DyedFactory(DyeType.DYE).setColor(DyeColor.GREEN).build(),FeatureType.LEVEL.getText() , "FEATURE_FEATURELEVEL_GREEN"));
		features.put(11, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_HELMET).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.HELMET.getText()));
		features.put(12, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bENDERMAN_TELEPORT"));
		features.put(13, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_BOOTS).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.BOOTS.getText()));
		features.put(14, new Feature(FeatureType.WEAPON,new ItemFactory(Material.WOOD_AXE).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_WOOD_AXE"));
		features.put(15, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_LEGGINGS).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.LEGGINGS.getText()));
		features.put(16, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bIRONGOLEM_HIT"));
		features.put(17, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_CHESTPLATE).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.CHESTPLATE.getText()));
		features.put(18, new Feature(FeatureType.WEAPON,new ItemFactory(Material.WOOD_SWORD).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_WOOD_SWORD"));
		features.put(19, new Feature(FeatureType.SKULL,KnockOut.getInstance().getPlayerHeadManager().getHead(1),FeatureType.SKULL.getText()));

		features.put(20, new Feature(FeatureType.LEVEL,new DyedFactory(DyeType.DYE).setColor(DyeColor.LIGHT_BLUE).build(),FeatureType.LEVEL.getText() , "FEATURE_FEATURELEVEL_BRIGHTBLUE"));
		features.put(21, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_HELMET).setColor(Color.AQUA).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.HELMET.getText()));
		features.put(22, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bVILLAGER_HIT"));
		features.put(23, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_BOOTS).setColor(Color.AQUA).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.BOOTS.getText()));
		features.put(24, new Feature(FeatureType.WEAPON,new ItemFactory(Material.STONE_HOE).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_STONE_HOE"));
		features.put(25, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_LEGGINGS).setColor(Color.AQUA).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.LEGGINGS.getText()));
		features.put(26, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bBLAZE_HIT"));
		features.put(27, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_CHESTPLATE).setColor(Color.AQUA).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.CHESTPLATE.getText()));
		features.put(28, new Feature(FeatureType.WEAPON,new ItemFactory(Material.STONE_AXE).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_STONE_AXE"));
		features.put(29, new Feature(FeatureType.SKULL,KnockOut.getInstance().getPlayerHeadManager().getHead(2),FeatureType.SKULL.getText()));

		features.put(30, new Feature(FeatureType.LEVEL,new DyedFactory(DyeType.DYE).setColor(DyeColor.CYAN).build(),FeatureType.LEVEL.getText() , "FEATURE_FEATURELEVEL_BLUE"));
		features.put(31, new Feature(FeatureType.ARMOR,new ItemFactory(Material.CHAINMAIL_HELMET).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , "FEATURE_FEATUREARMOR_CHAINMAIL_HELMET"));
		features.put(32, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bCAT_MEOW"));
		features.put(33, new Feature(FeatureType.ARMOR,new ItemFactory(Material.CHAINMAIL_BOOTS).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , "FEATURE_FEATUREARMOR_CHAINMAIL_BOOTS"));
		features.put(34, new Feature(FeatureType.WEAPON,new ItemFactory(Material.STONE_SWORD).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_STONE_SWORD"));
		features.put(35, new Feature(FeatureType.ARMOR,new ItemFactory(Material.CHAINMAIL_LEGGINGS).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText(), "FEATURE_FEATUREARMOR_CHAINMAIL_LEGGINGS"));
		features.put(36, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bARROW_HIT"));
		features.put(37, new Feature(FeatureType.ARMOR,new ItemFactory(Material.CHAINMAIL_CHESTPLATE).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText(), "FEATURE_FEATUREARMOR_CHAINMAIL_CHESTPLATE"));
		features.put(38, new Feature(FeatureType.WEAPON,new ItemFactory(Material.GOLD_HOE).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_GOLD_HOE"));
		features.put(39, new Feature(FeatureType.SKULL,KnockOut.getInstance().getPlayerHeadManager().getHead(3),FeatureType.SKULL.getText()));

		features.put(40, new Feature(FeatureType.LEVEL,new DyedFactory(DyeType.DYE).setColor(DyeColor.YELLOW).build(),FeatureType.LEVEL.getText() , "FEATURE_FEATURELEVEL_YELLOW"));
		features.put(41, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_HELMET).setColor(Color.ORANGE).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.HELMET.getText()));
		features.put(42, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bVILLAGER_YES"));
		features.put(43, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_BOOTS).setColor(Color.ORANGE).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.BOOTS.getText()));
		features.put(44, new Feature(FeatureType.WEAPON,new ItemFactory(Material.GOLD_AXE).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_GOLD_AXE"));
		features.put(45, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_LEGGINGS).setColor(Color.ORANGE).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.LEGGINGS.getText()));
		features.put(46, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bLAVA_POP"));
		features.put(47, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_CHESTPLATE).setColor(Color.ORANGE).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.CHESTPLATE.getText()));
		features.put(48, new Feature(FeatureType.WEAPON,new ItemFactory(Material.GOLD_SWORD).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_GOLD_SWORD"));
		features.put(49, new Feature(FeatureType.SKULL,KnockOut.getInstance().getPlayerHeadManager().getHead(4),FeatureType.SKULL.getText()));

		features.put(50, new Feature(FeatureType.LEVEL,new DyedFactory(DyeType.DYE).setColor(DyeColor.ORANGE).build(),FeatureType.LEVEL.getText() , "FEATURE_FEATURELEVEL_ORANGE"));
		features.put(51, new Feature(FeatureType.ARMOR,new ItemFactory(Material.GOLD_HELMET).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , "FEATURE_FEATUREARMOR_GOLD_HELMET"));
		features.put(52, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bCHICKEN_HURT"));
		features.put(53, new Feature(FeatureType.ARMOR,new ItemFactory(Material.GOLD_BOOTS).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , "FEATURE_FEATUREARMOR_GOLD_BOOTS"));
		features.put(54, new Feature(FeatureType.WEAPON,new ItemFactory(Material.IRON_HOE).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_IRON_HOE"));
		features.put(55, new Feature(FeatureType.ARMOR,new ItemFactory(Material.GOLD_LEGGINGS).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , "FEATURE_FEATUREARMOR_GOLD_LEGGINGS"));
		features.put(56, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bCOW_HURT"));
		features.put(57, new Feature(FeatureType.ARMOR,new ItemFactory(Material.GOLD_CHESTPLATE).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , "FEATURE_FEATUREARMOR_GOLD_CHESTPLATE"));
		features.put(58, new Feature(FeatureType.WEAPON,new ItemFactory(Material.IRON_AXE).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_IRON_AXE"));
		features.put(59, new Feature(FeatureType.SKULL,KnockOut.getInstance().getPlayerHeadManager().getHead(5),FeatureType.SKULL.getText()));

		features.put(60, new Feature(FeatureType.LEVEL,new DyedFactory(DyeType.DYE).setColor(DyeColor.RED).build(),FeatureType.LEVEL.getText() , "FEATURE_FEATURELEVEL_BRIGHTRED"));
		features.put(61, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_HELMET).setColor(Color.RED).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.HELMET.getText()));
		features.put(62, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bPIG_DEATH"));
		features.put(63, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_BOOTS).setColor(Color.RED).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.BOOTS.getText()));
		features.put(64, new Feature(FeatureType.WEAPON,new ItemFactory(Material.IRON_SWORD).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_IRON_SWORD"));
		features.put(65, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_LEGGINGS).setColor(Color.RED).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.LEGGINGS.getText()));
		features.put(66, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bWITHER_HURT"));
		features.put(67, new Feature(FeatureType.ARMOR,new LeatherArmorFactory(LeatherArmorType.LEATHER_CHESTPLATE).setColor(Color.RED).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , FeatureLeatherArmor.CHESTPLATE.getText()));
		features.put(68, new Feature(FeatureType.WEAPON,new ItemFactory(Material.DIAMOND_HOE).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_DIAMOND_HOE"));
		features.put(69, new Feature(FeatureType.SKULL,KnockOut.getInstance().getPlayerHeadManager().getHead(6),FeatureType.SKULL.getText()));

		features.put(70, new Feature(FeatureType.LEVEL,new DyedFactory(DyeType.DYE).setColor(DyeColor.RED).build(),FeatureType.LEVEL.getText() , "FEATURE_FEATURELEVEL_RED"));
		features.put(71, new Feature(FeatureType.ARMOR,new ItemFactory(Material.IRON_HELMET).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , "FEATURE_FEATUREARMOR_IRON_HELMET"));
		features.put(72, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bWOLF_HURT"));
		features.put(73, new Feature(FeatureType.ARMOR,new ItemFactory(Material.IRON_BOOTS).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , "FEATURE_FEATUREARMOR_IRON_BOOTS"));
		features.put(74, new Feature(FeatureType.WEAPON,new ItemFactory(Material.DIAMOND_AXE).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_DIAMOND_AXE"));
		features.put(75, new Feature(FeatureType.ARMOR,new ItemFactory(Material.IRON_LEGGINGS).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , "FEATURE_FEATUREARMOR_IRON_LEGGINGS"));
		features.put(76, new Feature(FeatureType.SOUND,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build(),FeatureType.SOUND.getText() , "#NT#§bCHICKEN_EGG_POP"));
		features.put(77, new Feature(FeatureType.ARMOR,new ItemFactory(Material.IRON_CHESTPLATE).setUnbreakable().addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build(),FeatureType.ARMOR.getText() , "FEATURE_FEATUREARMOR_IRON_CHESTPLATE"));
		features.put(78, new Feature(FeatureType.WEAPON,new ItemFactory(Material.DIAMOND_SWORD).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_ATTRIBUTES).build(),FeatureType.WEAPON.getText() , "FEATURE_FEATUREWEAPON_DIAMOND_SWORD"));
		features.put(79, new Feature(FeatureType.SKULL,KnockOut.getInstance().getPlayerHeadManager().getHead(7),FeatureType.SKULL.getText()));

		features.put(80, new Feature(null,new SkullFactory().setSkullType(SkullType.PLAYER).setSkullOwner("MHF_Question").build(),FeatureType.SKULL.getText() , "#NT#§7Comming soon: §ePrestige"));
		//TODO: Prestige
	}

	public Feature getFeature(int level){
		return features.get(level);
	}

	public enum FeatureType {
		LEVEL,
		ARMOR,
		WEAPON,
		SOUND,
		SKULL;

		public String getText() {
			return "FEATURE_FEATURETYPE_" + toString();
		}
	}

	public ArrayList<Feature> getFeatures(FeatureType featuretype){
		ArrayList<Feature> feature = new ArrayList<>();
		for(Feature f : features.values()){
			if(f.getFeaturetype() != null && f.getFeaturetype().equals(featuretype)){
				feature.add(f);
			}
		}
		return feature;
	}

	public int getNeededLevel(Feature feature){
		for(int i = 1; i <= features.values().size(); i++){
			if(features.get(i).equals(feature)){
				return i;
			}
		}

		return -1;
	}

	public Feature getFeature(ItemStack item){
		for(Feature feature : features.values()){
			if(feature.getIcon().equals(item)){
				return feature;
			}
		}

		return null;
	}

	public enum FeatureLeatherArmor{
		HELMET,
		CHESTPLATE,
		LEGGINGS,
		BOOTS;

		public String getText() {
			return "FEATURE_FEATUREARMOR_LEATHER_" + toString();
		}
	}
}


