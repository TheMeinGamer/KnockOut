package de.themeingamer.knockout.itemfactory;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class SkullFactory extends ItemFactory {

    public SkullFactory() {
        super(Material.SKULL_ITEM);
    }

    public SkullFactory setSkullType(SkullType type) {
        ItemMeta stackMeta = stack.getItemMeta();

        ItemStack item = new ItemStack(stack.getType(), stack.getAmount(), (short) type.getData());
        item.setItemMeta(stackMeta);
        this.stack = item;
        return this;
    }

    public SkullFactory setSkullOwner(String name) {
        SkullMeta meta = (SkullMeta) stack.getItemMeta();
        meta.setOwner(name);
        stack.setItemMeta(meta);
        return this;
    }

    public enum SkullType {

        SKELETON(0),
        SKELETON_WITHER(1),
        ZOMBIE(2),
        PLAYER(3),
        CREEPER(4);

        private int data;

        private SkullType(int data) {
            this.data = data;
        }

        public int getData() {
            return data;
        }
    }
}
