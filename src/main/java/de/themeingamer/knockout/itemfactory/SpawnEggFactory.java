package de.themeingamer.knockout.itemfactory;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.material.SpawnEgg;

public class SpawnEggFactory extends ItemFactory {

    public SpawnEggFactory() {
        super(Material.MONSTER_EGG);
    }

    public SpawnEggFactory setType(EntityType type) {
        SpawnEgg egg = new SpawnEgg(type);
        stack.setData(egg.toItemStack().getData());
        return this;
    }
}
