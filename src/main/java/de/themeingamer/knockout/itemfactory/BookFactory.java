package de.themeingamer.knockout.itemfactory;

import org.bukkit.Material;
import org.bukkit.inventory.meta.BookMeta;

public class BookFactory extends ItemFactory {

    public BookFactory() {
        super(Material.WRITTEN_BOOK);
    }

    public BookFactory addPage(String... page) {
        BookMeta meta = (BookMeta) getItemMeta();
        meta.addPage(page);
        stack.setItemMeta(meta);
        return this;
    }

    public BookFactory setAuthor(String author) {
        BookMeta meta = (BookMeta) getItemMeta();
        meta.setAuthor(author);
        stack.setItemMeta(meta);
        return this;
    }

    public BookFactory setTitle(String title) {
        BookMeta meta = (BookMeta) getItemMeta();
        meta.setTitle(title);
        stack.setItemMeta(meta);
        return this;
    }
}
