package de.themeingamer.knockout.itemfactory;

import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworkFactory extends ItemFactory {

    public FireworkFactory() {
        super(Material.FIREWORK);
    }

    public FireworkFactory addEffect(FireworkEffect effect) {
        FireworkMeta meta = getFireworkMeta();
        meta.addEffect(effect);
        this.stack.setItemMeta(meta);
        return this;
    }

    public FireworkFactory addEffects(FireworkEffect... effects) {
        FireworkMeta meta = getFireworkMeta();
        meta.addEffects(effects);
        this.stack.setItemMeta(meta);
        return this;
    }

    public FireworkFactory setPower(int power) {
        FireworkMeta meta = getFireworkMeta();
        meta.setPower(power);
        this.stack.setItemMeta(meta);
        return this;
    }

    public FireworkMeta getFireworkMeta() {
        return (FireworkMeta) this.stack.getItemMeta();
    }

    public Firework spawn(Location loc) {
        Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        fw.setFireworkMeta(getFireworkMeta());
        return fw;
    }

}
