package de.themeingamer.knockout.itemfactory;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class DyedFactory extends ItemFactory {

    private DyeType type;

	public DyedFactory(DyeType type) {
		super(type.getMaterial());
		this.type = type;
	}
	
	@SuppressWarnings("deprecation")
	public DyedFactory setColor(DyeColor dyeColor) {
		ItemStack item = new ItemStack(stack.getType(), stack.getAmount(), type.usesWoolID() ? dyeColor.getWoolData() : dyeColor.getDyeData());
		item.setItemMeta(stack.getItemMeta());
		this.stack = item;
		return this;
	}
	
	public enum DyeType {
		
		DYE(Material.INK_SACK, false),
		WOOL_BLOCK(Material.WOOL, true),
		WOOL_CARPET(Material.CARPET, true),
		CLAY_BLOCK(Material.STAINED_CLAY, true),
		CLAY_BLOCK_HARD(Material.HARD_CLAY, true),
		GLASS_BLOCK(Material.STAINED_GLASS, true),
		GLASS_PANE(Material.STAINED_GLASS_PANE, true);
		
		private Material m;
		private boolean woolID;
		
		private DyeType(Material m, boolean woolID) {
			this.m = m;
			this.woolID = woolID;
		}
		
		public boolean usesWoolID() {
			return this.woolID;
		}
		
		public Material getMaterial() {
			return this.m;
		}
	}
}