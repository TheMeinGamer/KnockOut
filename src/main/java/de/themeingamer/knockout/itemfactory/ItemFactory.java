package de.themeingamer.knockout.itemfactory;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.Arrays;
import java.util.List;

public class ItemFactory {

    protected ItemStack stack;

    public ItemFactory(Material material) {
        this.stack = new ItemStack(material);
    }

    public ItemFactory setAmount(int amount) {
        this.stack.setAmount(amount);
        return this;
    }

    public ItemFactory addEnchantment(Enchantment ench, int level) {
        this.stack.addUnsafeEnchantment(ench, level);
        return this;
    }

    public ItemFactory setDisplayName(String name) {
        ItemMeta meta = getItemMeta();
        meta.setDisplayName(name);
        this.stack.setItemMeta(meta);
        return this;
    }

    public ItemFactory addItemFlag(ItemFlag... flag) {
        ItemMeta meta = getItemMeta();
        meta.addItemFlags(flag);
        this.stack.setItemMeta(meta);
        return this;
    }

    public ItemFactory addLore(String... lore) {
        ItemMeta meta = getItemMeta();
        meta.setLore(Arrays.asList(lore));
        this.stack.setItemMeta(meta);
        return this;
    }

    public ItemFactory addLore(List<String> lore) {
        ItemMeta meta = getItemMeta();
        meta.setLore(lore);
        this.stack.setItemMeta(meta);
        return this;
    }

    public ItemFactory setDurability(int durability) {
        this.stack.setDurability((short) durability);
        return this;
    }

	public ItemFactory setUnbreakable() {
        ItemMeta meta = getItemMeta();
        meta.spigot().setUnbreakable(true);
        this.stack.setItemMeta(meta);
        return this;
    }

    public ItemFactory setItemMeta(ItemMeta meta) {
        this.stack.setItemMeta(meta);
        return this;
    }

    @SuppressWarnings("deprecation")
    public ItemFactory setItemMetaAsByte(byte data) {
        this.stack.setData(new MaterialData(this.stack.getType(), data));
        return this;
    }

    public ItemStack build() {
        return this.stack;
    }
    
    public String getDisplayName(){
    	return this.stack.getItemMeta().getDisplayName();
    }

    protected ItemMeta getItemMeta() {
        return this.stack.getItemMeta();
    }
    
    
}
