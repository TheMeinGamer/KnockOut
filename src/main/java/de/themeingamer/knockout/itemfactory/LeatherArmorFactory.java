package de.themeingamer.knockout.itemfactory;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class LeatherArmorFactory extends ItemFactory {

    public LeatherArmorFactory(LeatherArmorType type) {
        super(type.getMaterial());
    }

    public LeatherArmorFactory setColor(Color color) {
        LeatherArmorMeta meta = (LeatherArmorMeta) stack.getItemMeta();
        meta.setColor(color);
        stack.setItemMeta(meta);
        return this;
    }

    public enum LeatherArmorType {
        LEATHER_HELMET(Material.LEATHER_HELMET),
        LEATHER_CHESTPLATE(Material.LEATHER_CHESTPLATE),
        LEATHER_LEGGINGS(Material.LEATHER_LEGGINGS),
        LEATHER_BOOTS(Material.LEATHER_BOOTS);

        private Material material;

        private LeatherArmorType(Material material) {
            this.material = material;
        }

        public Material getMaterial() {
            return material;
        }
    }
}
