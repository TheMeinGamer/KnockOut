package de.themeingamer.knockout.itemfactory;

import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.inventory.meta.BannerMeta;

public class BannerFactory extends ItemFactory {

    public BannerFactory(Material mat) {
        super(mat);
    }

    public BannerFactory addLayer(Pattern layer) {
        BannerMeta meta = (BannerMeta) getMeta();
        meta.addPattern(layer);
        stack.setItemMeta(meta);
        return this;
    }

    private BannerMeta getMeta() {
        return (BannerMeta) getItemMeta();
    }
}