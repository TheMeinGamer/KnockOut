package de.themeingamer.knockout.main;

import java.io.File;
import java.sql.SQLException;

import de.themeingamer.knockout.featureinventory.FeatureInventory;
import de.themeingamer.knockout.features.FeatureManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.themeingamer.knockout.account.AccountManager;
import de.themeingamer.knockout.commands.*;
import de.themeingamer.knockout.listeners.*;
import de.themeingamer.knockout.managers.*;
import de.themeingamer.knockout.map.MapManager;
import de.themeingamer.knockout.mysql.MySQL;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class KnockOut extends JavaPlugin{
	
	private static KnockOut instance;
	
	private ConfigManager configmanager;
	private MapManager mapmanager;
	private AccountManager accountmanager;
	private PlayerHeadManager playerheadmanager;
	private DeathManager deathmanager;
	private TitleManager titlemanager;
	private LevelManager levelmanager;
	private FeatureManager featuremanager;
	private FeatureInventory featureInventory;
	
	private MySQL mysql;
	
	@Override
	public void onEnable() {
		instance = this;
		writeInConsole("§7\n\n\n"
				+ "   ▄█   ▄█▄ ███▄▄▄▄    ▄██████▄   ▄████████    ▄█   ▄█▄  ▄██████▄  ███    █▄      ███          \n"
				+ "  ███ ▄███▀ ███▀▀▀██▄ ███    ███ ███    ███   ███ ▄███▀ ███    ███ ███    ███ ▀█████████▄      \n"
				+ "  ███▐██▀   ███   ███ ███    ███ ███    █▀    ███▐██▀   ███    ███ ███    ███    ▀███▀▀██      \n"
				+ " ▄█████▀    ███   ███ ███    ███ ███         ▄█████▀    ███    ███ ███    ███     ███   ▀      \n"
				+ "▀▀█████▄    ███   ███ ███    ███ ███        ▀▀█████▄    ███    ███ ███    ███     ███          \n"
				+ "  ███▐██▄   ███   ███ ███    ███ ███    █▄    ███▐██▄   ███    ███ ███    ███     ███          \n"
				+ "  ███ ▀███▄ ███   ███ ███    ███ ███    ███   ███ ▀███▄ ███    ███ ███    ███     ███          \n"
				+ "  ███   ▀█▀  ▀█   █▀   ▀██████▀  ████████▀    ███   ▀█▀  ▀██████▀  ████████▀     ▄████▀        \n"
				+ "  ▀                                           ▀                                                \n"
				+ "                                                                                by TheMeinGamer\n"
				+ "																								    ");
		loadMySQL();
		loadManagers();
		loadListeners();
		loadCommands();
		writeInConsole(getPrefix() + "§7wurde gestartet");
	}

	@Override
	public void onDisable() {
		for(Player all : Bukkit.getOnlinePlayers()){
			accountmanager.updateAccount(all.getUniqueId().toString());
			all.kickPlayer(getPrefix() + "§cDer Server wurde neugestartet §7!");
		}

		if(mysql != null){
			mysql.closeConnection();
		}
	}
	
	public void writeInConsole(String message){
		Bukkit.getConsoleSender().sendMessage(message);
	}

	private void loadMySQL() {
		try {
			mysql = new MySQL(this,new File(getDataFolder().getPath(),"mysql.yml"));
			writeInConsole("§7[MySQL] Verbinung §aaufgebaut§7. Erstelle Tabellen falls fehlend...");
			mysql.queryUpdate("CREATE TABLE IF NOT EXISTS knockoutmaps (mapname VARCHAR(100), mapauthors VARCHAR(100),"
					+ " spawnlocation VARCHAR(100), hologramlocation VARCHAR(100), mapicon VARCHAR(100), nopvpheight INT, deathheight INT, spawnradius INT, state INT);");
			mysql.queryUpdate("CREATE TABLE IF NOT EXISTS knockoutstats (uuid VARCHAR(100),name VARCHAR(100), level INT, xp INT,"
					+ " customstick VARCHAR(100), deathsound VARCHAR(100), killsound VARCHAR(100), levelupsound VARCHAR(100));");
			mysql.queryUpdate("CREATE TABLE IF NOT EXISTS knockoutheads (level INT, uuid VARCHAR(100))");
			writeInConsole("§7[MySQL] Alle Tabellen sind erstellt!");
		} catch (ClassNotFoundException | SQLException e) {
			writeInConsole("§7[MySQL] Verbinung konnte §cnicht aufgebaut §7werden. Bitte korrigiere deine Angaben!");
			e.printStackTrace();
		}
	}

	private void loadManagers() {
		configmanager = new ConfigManager(new File(getDataFolder().getPath(),"config.yml"));
		mapmanager = new MapManager();
		accountmanager = new AccountManager();
		playerheadmanager = new PlayerHeadManager();
		deathmanager = new DeathManager();
		titlemanager = new TitleManager();
		levelmanager = new LevelManager();
		featuremanager = new FeatureManager();
		featureInventory = new FeatureInventory();
	}

	private void loadListeners() {
		PluginManager pm = Bukkit.getPluginManager();

		pm.registerEvents(new AsyncPlayerChatListener(), this);
		pm.registerEvents(new CancelListener(), this);
		pm.registerEvents(new EntityDamageByEntityListener(), this);
		pm.registerEvents(new InventoryClickListener(), this);
		pm.registerEvents(new PlayerInteractListener(), this);
		pm.registerEvents(new PlayerJoinListener(), this);
		pm.registerEvents(new PlayerLoginListener(), this);
		pm.registerEvents(new PlayerMoveListener(), this);
		pm.registerEvents(new PlayerQuitListener(), this);
		
	}

	private void loadCommands() {
		getCommand("forcemap").setExecutor(new Command_forcemap());
		getCommand("map").setExecutor(new Command_map());
	}
	
	
	public static KnockOut getInstance() {
		return instance;
	}
	
	public String getPrefix(){
		return configmanager.getData("prefix");
	}
	
	public ConfigManager getConfigManager() {
		return configmanager;
	}
	
	public MapManager getMapManager() {
		return mapmanager;
	}
	
	public AccountManager getAccountManager() {
		return accountmanager;
	}

	public PlayerHeadManager getPlayerHeadManager() {
		return playerheadmanager;
	}

	public DeathManager getDeathManager() {
		return deathmanager;
	}
	
	public TitleManager getTitleManager() {
		return titlemanager;
	}
	
	public LevelManager getLevelManager() {
		return levelmanager;
	}

	public FeatureManager getFeatureManager() {
		return featuremanager;
	}

	public FeatureInventory getFeatureInventory() {
		return featureInventory;
	}

	public MySQL getMySQL() {
		return mysql;
	}
	
}
