package de.themeingamer.knockout.account;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.Bukkit;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class AccountManager {
	
	private ArrayList<Account> accs;
	
	public AccountManager() {
		this.accs = new ArrayList<>();
	}
	
	public void createAccount(String uuid){
		KnockOut.getInstance().getMySQL().queryAsync("SELECT uuid FROM knockoutstats WHERE uuid='" + uuid + "';",(ResultSet rs) ->{
			try {
				if(rs.next()){
					addAccount(new Account(uuid));
				}else{
					addAccount(new Account(uuid,1,0,"STICK","NOTE_BASS", "ANVIL_BREAK", "LEVEL_UP"));
					KnockOut.getInstance().getMySQL().queryUpdateAsync("INSERT INTO knockoutstats (uuid,name,level,xp,customstick,deathsound,killsound,levelupsound) VALUES ('" + uuid + "','" + Bukkit.getPlayer(UUID.fromString(uuid)).getName() + "','1','0','STICK','ANVIL_BREAK','NOTE_BASS','LEVEL_UP');");
				}
			} catch (SQLException e) {
				KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(),"AccountManager.createAccount","UUID: " + uuid,e);
			}
		});
	}
	
	public void addAccount(Account acc){
		accs.add(acc);
	}
	
	public Account getAccount(String uuid){
		for(Account acc : accs){
			if(acc.getUUID().equals(uuid)){
				return acc;
			}
		}
		return null;
	}

	public void updateAccount(String uuid){
		Account acc = getAccount(uuid);
		if(acc != null){
			acc.update();
			acc.getAccountInventory().clear();
			accs.remove(acc);
		}
	}

	/*
	 * 0 = Name
	 * 1 = Kills
	 * 2 = Level
	 */
	public List<Object[]> getTop10List(){
		List<Object[]> list = new ArrayList<>();

		ResultSet rs = KnockOut.getInstance().getMySQL().query("SELECT * FROM knockoutstats ORDER BY level DESC, xp DESC LIMIT 10;");

		try {
			while (rs.next()){
				int level = rs.getInt("level");
				list.add(new Object[]{rs.getString("name"), KnockOut.getInstance().getLevelManager().getKills(level,rs.getInt("xp")), level});
            }
		} catch (SQLException e) {
			KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(), "AccountManager.getTop10List", "",e);
		}

		return list;
	}

}
