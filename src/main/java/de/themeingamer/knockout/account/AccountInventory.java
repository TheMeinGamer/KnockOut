package de.themeingamer.knockout.account;

import de.themeingamer.knockout.features.Feature;
import de.themeingamer.knockout.itemfactory.DyedFactory;
import de.themeingamer.knockout.itemfactory.ItemFactory;
import de.themeingamer.knockout.language.Language;
import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Creator: TheMeinGamer
 * Date: 25.04.17
 */
public class AccountInventory {

    private UUID uuid;
    private ItemStack placeHolder;
    private Object[] inventory;
    private HashMap<String, Inventory> invs;

    public AccountInventory(UUID uuid){
        this.uuid = uuid;
        this.placeHolder = new DyedFactory(DyedFactory.DyeType.GLASS_PANE).setColor(DyeColor.BLACK).setDisplayName(" ").build();
        this.inventory = new Object[2];
        this.invs = new HashMap<>();

    }

    public void openArmorEditInventory(){
        Inventory inv = invs.get("armorEdit");
        Player p = getPlayer();

        if(inv == null){
            inv = KnockOut.getInstance().getFeatureInventory().getArmorInventory().getArmorEditInventory(p);
            invs.put("armorEdit",inv);
        }

        p.openInventory(inv);
    }

    public void openSkullEditInventory(){
        Inventory inv = invs.get("skullEdit");
        Player p = getPlayer();

        if(inv == null){
            inv = KnockOut.getInstance().getFeatureInventory().getSkullInventory().getSkullEditInventory(p);
            invs.put("skullEdit",inv);
        }

        p.openInventory(inv);
    }

    public void openSoundEditInventory(){
        Inventory inv = invs.get("soundEdit");
        Player p = getPlayer();

        if(inv == null){
            inv = KnockOut.getInstance().getFeatureInventory().getSoundInventory().getSoundEditInventory(p);
            invs.put("soundEdit",inv);
        }

        p.openInventory(inv);
    }

    public void openWeaponEditInventory(){
        Inventory inv = invs.get("weaponEdit");
        Player p = getPlayer();

        if(inv == null){
            inv = KnockOut.getInstance().getFeatureInventory().getWeaponInventory().getWeaponEditInventory(p);
            invs.put("weaponEdit",inv);
        }

        p.openInventory(inv);
    }

    public void openFeatureInventory(Player p){
        Inventory inv = invs.get("feature");

        if(inv == null) {
            inv = Bukkit.createInventory(null, 9, Language.translate(p, "INVENTORY_FEATURE"));
            for (int i = 0; i < inv.getSize(); i++) {
                inv.setItem(i, placeHolder);
            }

            inv.setItem(1, new ItemFactory(Material.IRON_CHESTPLATE).setDisplayName(Language.translate(p, "ITEM_ARMOREDIT")).build());
            inv.setItem(3, new ItemFactory(Material.SKULL_ITEM).setDurability(3).setDisplayName(Language.translate(p, "ITEM_SKULLEDIT")).build());

            inv.setItem(5, new ItemFactory(Material.NOTE_BLOCK).setDisplayName(Language.translate(p, "ITEM_SOUNDEDIT")).build());
            inv.setItem(7, new ItemFactory(Material.STICK).setDisplayName(Language.translate(p, "ITEM_WEAPONEDIT")).build());

            invs.put("feature", inv);
        }

        p.openInventory(inv);
    }

    public Inventory openRewardIventory(Player p, int site){
        Inventory inv = Bukkit.createInventory(null,36,Language.translate(p,"INVENTORY_REWARDS",site));
        Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());

        int featurelevel = 27*site-26;

        for(int i = 0;i < 27; i++){
            if(featurelevel <= 80) {
                Feature feature = KnockOut.getInstance().getFeatureManager().getFeature(featurelevel);
                ItemStack item = feature.getIcon().clone();
                addItemMeta(item,(featurelevel <= acc.getLevel() ? "§a§l" : "§c") + "Level " + featurelevel,feature.getDescription(p));
                inv.setItem(i, item);
                featurelevel++;
            }else{
                inv.setItem(i,placeHolder);
            }
        }

        inv.setItem(27,new ItemFactory(Material.ARROW).setDisplayName((site < 2 ? "§7" : "§6") + Language.translate(p,"ITEM_BACK")).build());
        inv.setItem(35,new ItemFactory(Material.ARROW).setDisplayName((site < 3 ? "§6" : "§7") + Language.translate(p,"ITEM_FURTHER")).build());

        for(int i = 28; i < 35; i++){
            inv.setItem(i,placeHolder);
        }
        return inv;
    }

    public void openRewardInventory(int site){
        Player p = getPlayer();
        Object level = inventory[0];
        Account acc = getAccount();

        if(level == null){
            ArrayList<Inventory> list = new ArrayList<>();
            Inventory inv = openRewardIventory(p,1);
            list.add(inv);
            inventory[0] = acc.getLevel();
            inventory[1] = list;
            p.openInventory(inv);
        }else{
            if((int)level == acc.getLevel()){
                ArrayList<Inventory> list = (ArrayList<Inventory>) inventory[1];
                if(list.size()>=site){
                    p.openInventory(list.get(site-1));
                }else{
                    Inventory inv = openRewardIventory(p,site);
                    list.add(inv);
                    inventory[1] = list;
                    p.openInventory(inv);
                }
            }else{
                inventory[0] = null;
                openRewardInventory(1);
            }
        }
    }

    public void setLobbyInventory(){
        Player p = getPlayer();
        p.getInventory().clear();
        p.getInventory().setItem(0, new ItemFactory(Material.PAPER).setDisplayName(Language.translate(p,"ITEM_MAPVOTE")).build());
        p.getInventory().setItem(4, new ItemFactory(Material.GOLD_NUGGET).setDisplayName(Language.translate(p,"ITEM_FEATURE")).build());
        p.getInventory().setItem(8, new ItemFactory(Material.EXP_BOTTLE).setDisplayName(Language.translate(p,"ITEM_REWARD")).build());
    }

    public void setPvPInventory(){
        Player p = getPlayer();
        p.getInventory().clear();
        p.getInventory().setItem(0,KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString()).getCustomstick());
    }

    public void clear(){
        invs.clear();
    }


    private void addItemMeta(ItemStack item, String displayname, String lore){
        ArrayList<String> itemlore = new ArrayList<>();
        List<String> l = item.getItemMeta().getLore();
        itemlore.add(lore + (l == null ? "" : l.get(0)));
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayname);
        meta.setLore(itemlore);
        item.setItemMeta(meta);
    }

    private Account getAccount(){
        return  KnockOut.getInstance().getAccountManager().getAccount(uuid.toString());
    }

    private Player getPlayer(){
        return Bukkit.getServer().getPlayer(uuid);
    }
}
