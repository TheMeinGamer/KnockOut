package de.themeingamer.knockout.account;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.themeingamer.knockout.language.Language;
import de.themeingamer.knockout.utils.Hologram;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import de.themeingamer.knockout.main.KnockOut;
import de.themeingamer.knockout.itemfactory.ItemFactory;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class Account {
	
	private String uuid;
	private int level,xp;
	private ItemStack customstick;
	private Sound killsound, deathsound, levelupsound;
	private AccountInventory accountInventory;
	private Hologram hologram;
	
	public Account(String uuid){
		this.uuid = uuid;
		KnockOut.getInstance().getMySQL().queryAsync("SELECT * FROM knockoutstats WHERE uuid='" + this.uuid + "';", (ResultSet rs) -> {
			try {
				if(rs.next()){
					this.level = rs.getInt("level");
					this.xp = rs.getInt("xp");
					this.customstick = new ItemFactory(Material.valueOf(rs.getString("customstick"))).addEnchantment(Enchantment.KNOCKBACK,1).setUnbreakable().addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build();
					this.killsound = Sound.valueOf(rs.getString("killsound"));
					this.deathsound = Sound.valueOf(rs.getString("deathsound"));
					this.levelupsound = Sound.valueOf(rs.getString("levelupsound"));
				}
				this.accountInventory = new AccountInventory(UUID.fromString(uuid));
			} catch (SQLException e) {
				KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(),"Account","UUID: " + uuid,e);
			}
		});
	}
	
	public Account(String uuid, int level, int xp, String customstick, String killsound, String deathsound, String levelupsound){
		this.uuid = uuid;
		this.level = level;
		this.xp = xp;
		this.customstick = new ItemFactory(Material.valueOf(customstick)).addEnchantment(Enchantment.KNOCKBACK,1).setUnbreakable().addItemFlag(ItemFlag.HIDE_ENCHANTS).addItemFlag(ItemFlag.HIDE_UNBREAKABLE).build();
		this.killsound = Sound.valueOf(killsound);
		this.deathsound = Sound.valueOf(deathsound);
		this.levelupsound = Sound.valueOf(levelupsound);
		this.accountInventory = new AccountInventory(UUID.fromString(uuid));
	}
	
	public void update(){
		KnockOut.getInstance().getMySQL().queryUpdateAsync("UPDATE knockoutstats SET level='" + level + "" +
				"', name='" + Bukkit.getPlayer(UUID.fromString(uuid)).getName() +
				"', xp='" + xp +
				"', customstick='" + customstick.getType().toString() +
				"', killsound='" + killsound.toString() + 
				"', deathsound='" + deathsound.toString() + 
				"', levelupsound='" + levelupsound.toString() + "' WHERE uuid='" + uuid + "';");
	}

	public void setHologram(List<Object[]> data){
		List<String> lines = new ArrayList<>();
		int place = 1;

		for(Object[] objects : data){
			lines.add(Language.translate(Bukkit.getPlayer(UUID.fromString(uuid)),"HOLOGRAM_TOP10", place, objects[0], objects[1], objects[2]));
		    place++;
		}

		hologram = new Hologram(KnockOut.getInstance(),KnockOut.getInstance().getMapManager().getCurrentMap().getHologramLocation(), lines);
	}

	
	public String getUUID() {
		return uuid;
	}

	public int getLevel() {
		return level;
	}

	public int getXp() {
		return xp;
	}

	public ItemStack getCustomstick() {
		return customstick;
	}

	public Sound getKillsound() {
		return killsound;
	}

	public Sound getDeathsound() {
		return deathsound;
	}

	public Sound getLevelupsound() {
		return levelupsound;
	}

	public Hologram getHologram() {
		return hologram;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}

	public void setCustomstick(ItemStack customstick) {
		this.customstick = customstick;
	}

	public void setKillsound(Sound killsound) {
		this.killsound = killsound;
	}

	public void setDeathsound(Sound deathsound) {
		this.deathsound = deathsound;
	}

	public void setLevelupsound(Sound levelupsound) {
		this.levelupsound = levelupsound;
	}

	public AccountInventory getAccountInventory() {
		return accountInventory;
	}
}
