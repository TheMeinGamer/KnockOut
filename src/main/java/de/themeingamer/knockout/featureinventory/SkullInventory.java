package de.themeingamer.knockout.featureinventory;

import de.themeingamer.knockout.account.Account;
import de.themeingamer.knockout.features.Feature;
import de.themeingamer.knockout.features.FeatureManager;
import de.themeingamer.knockout.itemfactory.ItemFactory;
import de.themeingamer.knockout.language.Language;
import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

/**
 * Creator: TheMeinGamer
 * Date: 24.04.17
 */
public class SkullInventory {

    public Inventory getSkullEditInventory(Player p){
        Inventory inv = Bukkit.createInventory(null, 9, Language.translate(p,"INVENTORY_SKULLEDIT"));
        int slot = 0;

        for(Feature skull : KnockOut.getInstance().getFeatureManager().getFeatures(FeatureManager.FeatureType.SKULL)){
            ItemStack item = skull.getIcon().clone();
            String title = "§6" + ((SkullMeta)item.getItemMeta()).getOwner();
            KnockOut.getInstance().getFeatureInventory().addItemMeta(item,title,Language.translate(p,"EDIT_INFORMATION"),
                    Language.translate(p,"EDIT_LEVEL", KnockOut.getInstance().getFeatureManager().getNeededLevel(skull)));
            inv.setItem(slot,item);
            slot++;
        }
        inv.setItem(8, new ItemFactory(Material.BARRIER).setDisplayName(Language.translate(p,"ITEM_REMOVE_SKULL")).build());

        return inv;
    }

    public void handle(Player p, ItemStack item, String itemTitle){
        if (itemTitle.equals(Language.translate(p, "ITEM_REMOVE_SKULL"))) {
            if (p.getInventory().getHelmet() != null) {
                p.getInventory().setHelmet(null);
                p.playSound(p.getLocation(), Sound.LAVA_POP, 1, 1);
            }
        } else {
            Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());
            if (KnockOut.getInstance().getFeatureInventory().getNeededLevel(1, item) <= acc.getLevel()) {
                setSkull(p, item.clone());
            } else {
                p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
                p.sendMessage(KnockOut.getInstance().getPrefix() + Language.translate(p, "EDIT_LOWLEVEL"));
            }
        }
    }

    private void setSkull(Player p, ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        meta.setLore(null);
        item.setItemMeta(meta);

        p.getInventory().setHelmet(item);
        p.playSound(p.getLocation(), Sound.CLICK, 1, 1);
    }
}
