package de.themeingamer.knockout.featureinventory;

import de.themeingamer.knockout.account.Account;
import de.themeingamer.knockout.features.Feature;
import de.themeingamer.knockout.features.FeatureManager;
import de.themeingamer.knockout.itemfactory.DyedFactory;
import de.themeingamer.knockout.itemfactory.ItemFactory;
import de.themeingamer.knockout.language.Language;
import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

/**
 * Creator: TheMeinGamer
 * Date: 24.04.17
 */
public class SoundInventory {

    private ItemStack placeholder;

    public SoundInventory() {
        this.placeholder = new DyedFactory(DyedFactory.DyeType.GLASS_PANE).setColor(DyeColor.BLACK).setDisplayName(" ").build();
    }

    public Inventory getSoundEditInventory(Player p){
        Inventory inv = Bukkit.createInventory(null, 27, Language.translate(p,"INVENTORY_SOUNDEDIT"));

        for(int i = 0; i < inv.getSize(); i++){
            inv.setItem(i,placeholder);
        }

        int slot = 3;

        inv.setItem(0,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).setDisplayName("§bANVIL_BREAK").addLore(Language.translate(p,"EDIT_INFORMATION"), Language.translate(p,"EDIT_LEVEL").replace("%LEVEL%","0")).build());
        inv.setItem(1,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).setDisplayName("§bNOTE_BASS").addLore(Language.translate(p,"EDIT_INFORMATION"), Language.translate(p,"EDIT_LEVEL").replace("%LEVEL%","0")).build());
        inv.setItem(2,new ItemFactory(Material.RECORD_3).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).setDisplayName("§bLEVEL_UP").addLore(Language.translate(p,"EDIT_INFORMATION"), Language.translate(p,"EDIT_LEVEL").replace("%LEVEL%","0")).build());


        for(Feature sound : KnockOut.getInstance().getFeatureManager().getFeatures(FeatureManager.FeatureType.SOUND)){
            ItemStack item = sound.getIcon().clone();
            String title = sound.getDescription(p).split(" ")[1];
            KnockOut.getInstance().getFeatureInventory().addItemMeta(item,title,Language.translate(p,"EDIT_INFORMATION"), Language.translate(p,"EDIT_LEVEL", KnockOut.getInstance().getFeatureManager().getNeededLevel(sound)));
            inv.setItem(slot,item);
            slot++;
        }

        return inv;
    }

    public void openSecondSoundIventory(Player p, String itemTitle){

        Inventory inv = Bukkit.createInventory(null,9, Language.translate(p, "INVENTORY_SECONDSOUNDEDIT"));

        for(int i = 0; i < inv.getSize(); i++){
            inv.setItem(i,placeholder);
        }

        String soundName = ChatColor.stripColor(itemTitle);
        String chooseLore = Language.translate(p, "SECONDSOUNDEDIT_CHOOSEAS", soundName);

        inv.setItem(0, new ItemFactory(Material.RECORD_3).setDisplayName(Language.translate(p, "SECONDSOUNDEDIT_LISTEN", soundName)).addItemFlag(ItemFlag.HIDE_POTION_EFFECTS).build());
        inv.setItem(2, new DyedFactory(DyedFactory.DyeType.WOOL_BLOCK).setColor(DyeColor.GREEN).setDisplayName(chooseLore).addLore(Language.translate(p, "SECONDSOUNDEDIT_KILLSOUND")).build());
        inv.setItem(4, new DyedFactory(DyedFactory.DyeType.WOOL_BLOCK).setColor(DyeColor.RED).setDisplayName(chooseLore).addLore(Language.translate(p, "SECONDSOUNDEDIT_DEATHSOUND")).build());
        inv.setItem(6, new DyedFactory(DyedFactory.DyeType.WOOL_BLOCK).setColor(DyeColor.ORANGE).setDisplayName(chooseLore).addLore(Language.translate(p, "SECONDSOUNDEDIT_LEVELUPSOUND")).build());

        p.openInventory(inv);
    }

    public void handle(Player p, ItemStack item, String itemTitle){

        if(item.getType().equals(Material.WOOL)){
            Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());
            if (getNeededLevel(p, itemTitle.split(" ")[0]) <= acc.getLevel()) {
                setSound(p, item);
            } else {
                p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
                p.sendMessage(KnockOut.getInstance().getPrefix() + Language.translate(p, "EDIT_LOWLEVEL"));
            }
        }else{
            p.playSound(p.getLocation(),Sound.valueOf(ChatColor.stripColor(item.getItemMeta().getDisplayName().split(" ")[0])), 1f, 1f);
        }
    }

    private void setSound(Player p, ItemStack item){
        Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());
        Sound sound = Sound.valueOf(ChatColor.stripColor(item.getItemMeta().getDisplayName().split(" ")[0]));
        String lore = item.getItemMeta().getLore().get(0);

        if(lore.equals(Language.translate(p, "SECONDSOUNDEDIT_KILLSOUND"))){
            acc.setKillsound(sound);
        }else if(lore.equals(Language.translate(p, "SECONDSOUNDEDIT_DEATHSOUND"))){
            acc.setDeathsound(sound);
        }else{
            acc.setLevelupsound(sound);
        }

        p.sendMessage(Language.translate(p, "SETSOUND", ChatColor.stripColor(lore), sound.toString()));
        p.playSound(p.getLocation(), sound, 1f, 1f);

    }

    private int getNeededLevel(Player p, String soundName){
        soundName = ChatColor.stripColor(soundName);
        for(Feature sound : KnockOut.getInstance().getFeatureManager().getFeatures(FeatureManager.FeatureType.SOUND)){
            if(sound.getDescription(p).endsWith(soundName)){
                return KnockOut.getInstance().getFeatureManager().getNeededLevel(sound);
            }
        }

        return 1;
    }
}
