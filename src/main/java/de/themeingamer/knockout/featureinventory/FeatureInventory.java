package de.themeingamer.knockout.featureinventory;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Creator: TheMeinGamer
 * Date: 24.04.17
 */
public class FeatureInventory {

    private ArmorInventory armorInventory;
    private SkullInventory skullInventory;
    private SoundInventory soundInventory;
    private WeaponInventory weaponInventory;

    public FeatureInventory(){
        this.armorInventory = new ArmorInventory();
        this.skullInventory = new SkullInventory();
        this.soundInventory = new SoundInventory();
        this.weaponInventory = new WeaponInventory();
    }

    public ArmorInventory getArmorInventory() {
        return armorInventory;
    }

    public SkullInventory getSkullInventory() {
        return skullInventory;
    }

    public SoundInventory getSoundInventory() {
        return soundInventory;
    }

    public WeaponInventory getWeaponInventory() {
        return weaponInventory;
    }

    public void addItemMeta(ItemStack item, String displayname, String... lore){
        ArrayList<String> itemlore = new ArrayList<>();
        itemlore.addAll(Arrays.asList(lore));
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayname);
        meta.setLore(itemlore);
        item.setItemMeta(meta);
    }

    public int getNeededLevel(int line, ItemStack item){
        return Integer.parseInt(ChatColor.stripColor(item.getItemMeta().getLore().get(line).split(" ")[2]));
    }
}
