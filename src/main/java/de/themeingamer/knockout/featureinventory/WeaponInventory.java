package de.themeingamer.knockout.featureinventory;

import de.themeingamer.knockout.account.Account;
import de.themeingamer.knockout.features.Feature;
import de.themeingamer.knockout.features.FeatureManager;
import de.themeingamer.knockout.itemfactory.DyedFactory;
import de.themeingamer.knockout.itemfactory.ItemFactory;
import de.themeingamer.knockout.language.Language;
import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Creator: TheMeinGamer
 * Date: 25.04.17
 */
public class WeaponInventory {

    /*
        0 = Other [Stick...]
        1 = Hoe
        2 = Axe
        3 = Sword
     */
    private int[] slots;
    private ItemStack placeholder;


    public WeaponInventory() {
        this.placeholder = new DyedFactory(DyedFactory.DyeType.GLASS_PANE).setColor(DyeColor.BLACK).setDisplayName(" ").build();
    }

    public Inventory getWeaponEditInventory(Player p){
        Inventory inv = Bukkit.createInventory(null, 36, Language.translate(p,"INVENTORY_WEAPONEDIT"));

        for(int i = 0; i < inv.getSize(); i++){
            inv.setItem(i,placeholder);
        }

        slots = new int[]{0,0,0,0};

        setItem(inv, new ItemFactory(Material.STICK).setDisplayName(Language.translate(p, "FEATURE_FEATUREWEAPON_STICK")).setUnbreakable().addEnchantment(Enchantment.KNOCKBACK, 1).addItemFlag(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_UNBREAKABLE).addLore(Language.translate(p,"EDIT_INFORMATION"), Language.translate(p,"EDIT_LEVEL").replace("%LEVEL%", "0")).build(),0);


        for(Feature weapon : KnockOut.getInstance().getFeatureManager().getFeatures(FeatureManager.FeatureType.WEAPON)){

            ItemStack item = weapon.getIcon().clone();
            int type = 0;

            if(item.getType().toString().endsWith("_HOE")){
                type = 1;
            }else if(item.getType().toString().endsWith("_AXE")){
                type = 2;
            }else if(item.getType().toString().endsWith("_SWORD")){
                type = 3;
            }

            String title = weapon.getDescription(p).split(" ")[1];
            KnockOut.getInstance().getFeatureInventory().addItemMeta(item,title,Language.translate(p,"EDIT_INFORMATION"), Language.translate(p,"EDIT_LEVEL", KnockOut.getInstance().getFeatureManager().getNeededLevel(weapon)));
            setItem(inv, item, type);
        }

        return inv;
    }

    public void handle(Player p, ItemStack item, String itemTitle){
        Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());
        if (KnockOut.getInstance().getFeatureInventory().getNeededLevel(1, item) <= acc.getLevel()) {
            setWeapon(p, item.clone(), itemTitle);
        } else {
            p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
            p.sendMessage(KnockOut.getInstance().getPrefix() + Language.translate(p, "EDIT_LOWLEVEL"));
        }
    }

    private void setWeapon(Player p, ItemStack item, String itemTitle) {
        ItemMeta meta = item.getItemMeta();
        meta.setLore(null);
        meta.setDisplayName(null);
        item.setItemMeta(meta);

        Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());
        acc.setCustomstick(item);

        p.playSound(p.getLocation(), Sound.CLICK, 1, 1);
        p.sendMessage(Language.translate(p, "SETWEAPON", ChatColor.stripColor(itemTitle)));
    }

    private void setItem(Inventory inv, ItemStack item, int type){
        int slot = slots[type];
        inv.setItem(slot + (type*9),item);
        slots[type] = (slot+1);
    }
}
