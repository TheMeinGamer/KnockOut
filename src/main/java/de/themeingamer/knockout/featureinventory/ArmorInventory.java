package de.themeingamer.knockout.featureinventory;

import de.themeingamer.knockout.account.Account;
import de.themeingamer.knockout.features.Feature;
import de.themeingamer.knockout.features.FeatureManager;
import de.themeingamer.knockout.itemfactory.DyedFactory;
import de.themeingamer.knockout.itemfactory.ItemFactory;
import de.themeingamer.knockout.language.Language;
import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Creator: TheMeinGamer
 * Date: 24.04.17
 */
public class ArmorInventory {

    private ItemStack placeholder;

    public ArmorInventory() {
        this.placeholder = new DyedFactory(DyedFactory.DyeType.GLASS_PANE).setColor(DyeColor.BLACK).setDisplayName(" ").build();
    }

    public Inventory getArmorEditInventory(Player p){
        Inventory inv = Bukkit.createInventory(null, 54, Language.translate(p,"INVENTORY_ARMOREDIT"));

        for(int i = 0; i < inv.getSize(); i++){
            inv.setItem(i,placeholder);
        }

        int slot = 0;
        int breakSlot = 4;

        for(Feature armor : KnockOut.getInstance().getFeatureManager().getFeatures(FeatureManager.FeatureType.ARMOR)){
            ItemStack item = armor.getIcon().clone();
            String title = armor.getDescription(p).split(" ")[1];
            KnockOut.getInstance().getFeatureInventory().addItemMeta(item,title,Language.translate(p,"EDIT_INFORMATION"),
                    Language.translate(p,"ARMOREDIT_TYPE",  ChatColor.stripColor(title)),
                    Language.translate(p,"EDIT_LEVEL", KnockOut.getInstance().getFeatureManager().getNeededLevel(armor)));
            inv.setItem(slot, item);
            slot++;
            if(slot == breakSlot){
                slot ++;
                breakSlot += 9;
            }
        }

        inv.setItem(47, new ItemFactory(Material.BARRIER).setDisplayName(Language.translate(p,"ITEM_REMOVE_HELMET")).build());
        inv.setItem(48, new ItemFactory(Material.BARRIER).setDisplayName(Language.translate(p,"ITEM_REMOVE_CHESTPLATE")).build());
        inv.setItem(50, new ItemFactory(Material.BARRIER).setDisplayName(Language.translate(p,"ITEM_REMOVE_LEGGINGS")).build());
        inv.setItem(51, new ItemFactory(Material.BARRIER).setDisplayName(Language.translate(p,"ITEM_REMOVE_BOOTS")).build());

        return inv;
    }

    public void handle(Player p, ItemStack item, String itemTitle){

        if(itemTitle.endsWith(Language.translate(p, "ITEM_REMOVE_HELMET").split(" ")[1])){
            removeArmor(p,itemTitle);
        }else{
            Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());
            if(KnockOut.getInstance().getFeatureInventory().getNeededLevel(2,item) <= acc.getLevel()){
                setArmor(p,item.clone());
            }else{
                p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
                p.sendMessage(KnockOut.getInstance().getPrefix() + Language.translate(p, "EDIT_LOWLEVEL"));
            }
        }
    }


    private void removeArmor(Player p, String itemdisplayname) {
        if(itemdisplayname.equals(Language.translate(p,"ITEM_REMOVE_HELMET"))){
            if(p.getInventory().getHelmet() != null) {
                p.getInventory().setHelmet(null);
                p.playSound(p.getLocation(), Sound.LAVA_POP, 1, 1);
            }
        }else if(itemdisplayname.equals(Language.translate(p,"ITEM_REMOVE_CHESTPLATE"))){
            if(p.getInventory().getChestplate() != null) {
                p.getInventory().setChestplate(null);
                p.playSound(p.getLocation(), Sound.LAVA_POP, 1, 1);
            }
        }else if(itemdisplayname.equals(Language.translate(p,"ITEM_REMOVE_LEGGINGS"))){
            if(p.getInventory().getLeggings() != null) {
                p.getInventory().setLeggings(null);
                p.playSound(p.getLocation(), Sound.LAVA_POP, 1, 1);
            }
        }else if(itemdisplayname.equals(Language.translate(p,"ITEM_REMOVE_BOOTS"))){
            if(p.getInventory().getBoots() != null) {
                p.getInventory().setBoots(null);
                p.playSound(p.getLocation(), Sound.LAVA_POP, 1, 1);
            }
        }
    }

    private void setArmor(Player p, ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        meta.setLore(null);
        meta.setDisplayName(null);
        item.setItemMeta(meta);

        if(item.getType().toString().endsWith("HELMET")){
            p.getInventory().setHelmet(item);
        } else if(item.getType().toString().endsWith("CHESTPLATE")){
            p.getInventory().setChestplate(item);
        } else if(item.getType().toString().endsWith("LEGGINGS")){
            p.getInventory().setLeggings(item);
        } else if(item.getType().toString().endsWith("BOOTS")){
            p.getInventory().setBoots(item);
        }
        p.playSound(p.getLocation(), Sound.CLICK, 1, 1);
    }

}
