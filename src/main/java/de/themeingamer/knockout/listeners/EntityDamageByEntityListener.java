package de.themeingamer.knockout.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import de.themeingamer.knockout.main.KnockOut;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class EntityDamageByEntityListener implements Listener{
	
	@EventHandler
	
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e){
		if(e.getEntity() instanceof Player && e.getDamager() instanceof Player){
			Player target = (Player) e.getEntity();
			Player damager = (Player) e.getDamager();
			e.setDamage(0);
			if(target.getLocation().getY() > KnockOut.getInstance().getMapManager().getCurrentMap().getPvpheight() || 
					damager.getLocation().getY() > KnockOut.getInstance().getMapManager().getCurrentMap().getPvpheight()){
				e.setCancelled(true);
			}else{
				KnockOut.getInstance().getDeathManager().addHit(target, damager);
			}
			
		}
	}

}
