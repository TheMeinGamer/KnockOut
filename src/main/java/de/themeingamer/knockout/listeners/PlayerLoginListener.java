package de.themeingamer.knockout.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import de.themeingamer.knockout.main.KnockOut;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class PlayerLoginListener implements Listener{

	@EventHandler
	
	public void onLogin(PlayerLoginEvent e){
		KnockOut.getInstance().getAccountManager().createAccount(e.getPlayer().getUniqueId().toString());
	}
}
