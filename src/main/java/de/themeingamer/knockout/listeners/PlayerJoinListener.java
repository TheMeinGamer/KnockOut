package de.themeingamer.knockout.listeners;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;

import de.themeingamer.knockout.main.KnockOut;
import de.themeingamer.knockout.account.Account;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class PlayerJoinListener implements Listener{

	@EventHandler
	
	public void onJoin(PlayerJoinEvent e){
		e.setJoinMessage(null);
		
		Player p = e.getPlayer();

        p.teleport(KnockOut.getInstance().getMapManager().getCurrentMap().getPlayerSpawn());
		resetPlayer(p);
		setLevel(p);
		
	}
	
	private void resetPlayer(Player p){
		p.setLevel(0);
        p.setExp(0);
        p.setHealth(20);
        p.setFoodLevel(20);
        p.setFallDistance(0F);
		p.getInventory().clear();
		KnockOut.getInstance().getPlayerHeadManager().checkHead(p);
        p.setGameMode(GameMode.SURVIVAL);
        for (PotionEffect effect : p.getActivePotionEffects()) {
        	p.removePotionEffect(effect.getType());
        }
        p.setFallDistance(0F);
        p.setFireTicks(0);
        p.setFlying(false);
        p.setAllowFlight(false);
	}
	
	private void setLevel(Player p){
		Bukkit.getScheduler().scheduleSyncDelayedTask(KnockOut.getInstance(), () -> {
            Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());
            if(acc == null || acc.getLevel() == 0){
                setLevel(p);
            }else{
                acc.setHologram(KnockOut.getInstance().getMapManager().getMapRotation().getData());
                acc.getHologram().showPlayer(p);
                p.setLevel(acc.getLevel());
                p.setExp(acc.getXp() / (float)KnockOut.getInstance().getLevelManager().getNeededXP(acc.getLevel()));
                acc.getAccountInventory().setLobbyInventory();
                for(Player all : Bukkit.getOnlinePlayers()){
                    p.showPlayer(all);
                    all.showPlayer(p);
                }
            }

        },20);
	}
}
