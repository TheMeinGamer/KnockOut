package de.themeingamer.knockout.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import de.themeingamer.knockout.main.KnockOut;
import de.themeingamer.knockout.account.Account;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class AsyncPlayerChatListener implements Listener{

	@EventHandler
	
	public void onChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		e.setFormat("§8[§7" + getColor(p) + "§8] §7" + p.getName() + "§r §8» §7" + e.getMessage().replace("%", "Prozent"));
	}
	
	private String getColor(Player p){
		Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());
		int level = acc.getLevel();
		if(level < 10) {
			return "§a" + level;
		} else if(level < 20) {
			return "§2" + level;
		} else if(level < 30) {
			return "§b" + level;
		} else if(level < 40) {
			return "§3" + level;
		} else if(level < 50) {
			return "§e" + level;
		} else if(level < 60) {
			return "§6" + level;
		} else if(level < 70) {
			return "§c" + level;
		} else {
			return "§4" + level;
		}
	}
}
