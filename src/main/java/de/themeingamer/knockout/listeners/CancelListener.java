package de.themeingamer.knockout.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class CancelListener implements Listener{
	
	@EventHandler
	
	public void onFoodLevelChange(FoodLevelChangeEvent e){
		e.setCancelled(true);
	}
	
	
	@EventHandler
	
	public void onWeatherChange(WeatherChangeEvent e){
		e.setCancelled(true);
	}
	
	
	@EventHandler
	
	public void onPlayerAchievementAwarded(PlayerAchievementAwardedEvent e){
		e.setCancelled(true);
	}
	
	
	@EventHandler
	
	public void onLeavesDecay(LeavesDecayEvent e){
		e.setCancelled(true);
	}
	
	
	@EventHandler
	
	public void onBockPlace(BlockPlaceEvent e){
		e.setCancelled(true);
	}
	
	
	@EventHandler
	
	public void onBlockBreak(BlockBreakEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	
	public void onHangingBreak(HangingBreakEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		if(e.getCause() != DamageCause.ENTITY_ATTACK && e.getCause() != DamageCause.VOID && e.getCause() != DamageCause.CUSTOM) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	
	public void onDrop(PlayerDropItemEvent e){
		e.setCancelled(true);
	}
	


}
