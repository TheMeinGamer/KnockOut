package de.themeingamer.knockout.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import de.themeingamer.knockout.main.KnockOut;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class PlayerInteractListener implements Listener{
	
	@EventHandler
	
	public void onInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if(e.getItem() != null && e.getItem().getItemMeta() != null){
			e.setCancelled(true);
			switch(e.getItem().getType()){
				case PAPER:
					KnockOut.getInstance().getMapManager().getMapVote().openInventory(p);
					break;
				case GOLD_NUGGET:
					KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString()).getAccountInventory().openFeatureInventory(p);
					break;
				case EXP_BOTTLE:
					KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString()).getAccountInventory().openRewardInventory(1);
					break;
				default:
					e.setCancelled(false);
					break;
				}
				
		}
	}

}
