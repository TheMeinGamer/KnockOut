package de.themeingamer.knockout.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.themeingamer.knockout.main.KnockOut;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class PlayerQuitListener implements Listener{

	@EventHandler
	
	public void onQuit(PlayerQuitEvent e){
		e.setQuitMessage(null);
		
		Player p = e.getPlayer();
		KnockOut.getInstance().getDeathManager().onQuit(p);
		KnockOut.getInstance().getAccountManager().updateAccount(p.getUniqueId().toString());
	}
}
