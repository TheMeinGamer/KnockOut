package de.themeingamer.knockout.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import de.themeingamer.knockout.main.KnockOut;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class PlayerMoveListener implements Listener{
	
	@EventHandler
	
	public void onMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		if(p.getWorld().equals(KnockOut.getInstance().getMapManager().getCurrentMap().getSpawn().getWorld()) && p.getLocation().getY() < KnockOut.getInstance().getMapManager().getCurrentMap().getDeathheight()){
			KnockOut.getInstance().getMapManager().getCurrentMap().death(p);
		}else if(p.getLocation().getY() < KnockOut.getInstance().getMapManager().getCurrentMap().getPvpheight()){
			if(p.getInventory().getItem(0) != null && p.getInventory().getItem(0).getType().equals(Material.PAPER)){
				KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString()).getAccountInventory().setPvPInventory();
				p.closeInventory();
			}
		}
	}

}
