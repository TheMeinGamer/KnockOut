package de.themeingamer.knockout.listeners;

import de.themeingamer.knockout.account.Account;
import de.themeingamer.knockout.account.AccountInventory;
import de.themeingamer.knockout.language.Language;
import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class InventoryClickListener implements Listener{
	
	@EventHandler
	
	public void onInventoryClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		e.setCancelled(true);
		if(e.getClickedInventory() != null && e.getCurrentItem() != null && e.getCurrentItem().getItemMeta() != null){
			ItemStack item = e.getCurrentItem();
			String inventoryTitle = e.getClickedInventory().getTitle();
			String itemTitle = item.getItemMeta().getDisplayName();
			if(item.getType().equals(Material.STAINED_GLASS_PANE) && item.getItemMeta().getDisplayName().equals(" ")){
				return;
			}
			if(inventoryTitle.equals(Language.translate(p,"INVENTORY_ALREADYVOTED")) ||
                    inventoryTitle.equals(Language.translate(p,"INVENTORY_VOTE")) ||
                    inventoryTitle.equals(Language.translate(p,"INVENTORY_VOTE_MAPSET"))){
					KnockOut.getInstance().getMapManager().getMapVote().addVote(p, item);
			}else if(inventoryTitle.startsWith(Language.translate(p,"INVENTORY_REWARDS").replace("{0}",""))){
				switch (item.getType()){
					case ARROW:
						String itemtitle = item.getItemMeta().getDisplayName();
						Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());
						if(itemtitle.contains("⇉")){
							if(itemtitle.startsWith("§6")){
								acc.getAccountInventory().openRewardInventory(Integer.parseInt(e.getInventory().getTitle().substring(inventoryTitle.length()-1,inventoryTitle.length())) + 1);
							}else{
								p.playSound(p.getLocation(), Sound.WOLF_WHINE, 1F, 1F);
							}
						}else{
							if(itemtitle.startsWith("§6")){
								acc.getAccountInventory().openRewardInventory(Integer.parseInt(e.getInventory().getTitle().substring(inventoryTitle.length()-1,inventoryTitle.length())) - 1);
							}else{
								p.playSound(p.getLocation(), Sound.WOLF_WHINE, 1F, 1F);
							}
						}
						break;
					default:
						break;
				}
			}else if(inventoryTitle.startsWith(Language.translate(p, "INVENTORY_FEATURE"))){
				AccountInventory accountInventory = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString()).getAccountInventory();
				switch (item.getType()){
					case IRON_CHESTPLATE:
						accountInventory.openArmorEditInventory();
						break;
					case SKULL_ITEM:
						accountInventory.openSkullEditInventory();
						break;
					case NOTE_BLOCK:
						accountInventory.openSoundEditInventory();
						break;
					case STICK:
						accountInventory.openWeaponEditInventory();
						break;
					default:
						break;
				}
			}else if(inventoryTitle.startsWith(Language.translate(p, "INVENTORY_ARMOREDIT"))){
				KnockOut.getInstance().getFeatureInventory().getArmorInventory().handle(p,item,itemTitle);
			}else if(inventoryTitle.startsWith(Language.translate(p, "INVENTORY_SKULLEDIT"))) {
                KnockOut.getInstance().getFeatureInventory().getSkullInventory().handle(p,item,itemTitle);
			}else if(inventoryTitle.startsWith(Language.translate(p, "INVENTORY_SOUNDEDIT"))) {
				KnockOut.getInstance().getFeatureInventory().getSoundInventory().openSecondSoundIventory(p, itemTitle);
			}else if(inventoryTitle.startsWith(Language.translate(p, "INVENTORY_SECONDSOUNDEDIT"))) {
				KnockOut.getInstance().getFeatureInventory().getSoundInventory().handle(p,item,itemTitle);
			}else if(inventoryTitle.startsWith(Language.translate(p, "INVENTORY_WEAPONEDIT"))) {
				KnockOut.getInstance().getFeatureInventory().getWeaponInventory().handle(p,item,itemTitle);
			}

		}
	}
}
