package de.themeingamer.knockout.managers;

import java.util.HashMap;

import de.themeingamer.knockout.language.Language;
import org.bukkit.entity.Player;

import de.themeingamer.knockout.main.KnockOut;
import de.themeingamer.knockout.account.Account;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class LevelManager {

	private HashMap<Integer,Integer> neededxp;
	
	public LevelManager() {
		this.neededxp = new HashMap<>();
		loadXp();
	}

	private void loadXp() {
		int xp = 6;
		for(int level = 1 ; level <= 80; level++){
			if(level <= 20){
				xp = (int) Math.round(xp*1.115);
			}else if (level <= 40){
				xp = (int) Math.round(xp*1.095);
			}else if(level <= 60){
				xp = (int) Math.round(xp*1.075);
			}else{
				xp = (int) Math.round(xp*1.055);
			}
			neededxp.put(level, xp);
		}
	}
	
	public void canLevelUp(Player p){
		Account acc = KnockOut.getInstance().getAccountManager().getAccount(p.getUniqueId().toString());
		int needxp = neededxp.get(acc.getLevel());
		if(acc.getXp() >= needxp){
			acc.setLevel(acc.getLevel() + 1);
			acc.setXp(acc.getXp() - needxp);
			p.setLevel(acc.getLevel());
			p.playSound(p.getLocation(),acc.getLevelupsound(),1F,1F);
            p.sendMessage(Language.translate(p, "LEVELUP_MESSAGE"));
            p.sendMessage(Language.translate(p, "LEVELUP_NEWLEVEL", acc.getLevel()));
            p.sendMessage(Language.translate(p,"LEVELUP_NEEDEDKILLS", getNeededXP(acc.getLevel()) - acc.getXp()));
		}
		p.setExp(acc.getXp() / (float)neededxp.get(acc.getLevel()));
	}
	
	public int getNeededXP(int level){
		return neededxp.get(level);
	}

	public int getKills(int level, int xp){
		int kills = xp;

		for(int i = 1; i <= level; i++){
			kills += neededxp.get(i);
		}

		return kills;
	}
	
}
