package de.themeingamer.knockout.managers;

import java.util.*;

import de.themeingamer.knockout.language.Language;
import de.themeingamer.knockout.utils.Hologram;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import de.themeingamer.knockout.main.KnockOut;

import javax.security.auth.callback.LanguageCallback;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class DeathManager {
	
	
	private HashMap<Player,Object[]> hits;
	private HashMap<Player,Integer> killstreak;
	private Random r;
	
	public DeathManager() {
		hits = new HashMap<>();
		killstreak = new HashMap<>();
		r = new Random();
	}

	
	public void addHit(Player target, Player damager){
		Object[] o = hits.get(target);
		if(o != null){
			hits.remove(target, o);
		}
		hits.put(target, new Object[]{damager,System.currentTimeMillis()});
	}
	
	public Player getDamager(Player target){
		Object[] o = hits.get(target);
		if(o == null){
			return null;
		}
		hits.remove(target, hits.get(target));
		return (System.currentTimeMillis() - (long)o[1] <= 3000 ? (Player)o[0] : null);
	}
	
	public void kill(Player target, Player killer){
		if(killer != null){
			int kills = 1;
			if(killstreak.get(killer) != null){
				kills += killstreak.get(killer);
				killstreak.remove(killer);
				manageKillStreakMessage(killer,kills);
			}
			killstreak.put(killer, kills);
			getHologram(killer, target).showPlayerTemp(killer, 20*3);
		}
		killstreak.remove(target);

	}

	public void onQuit(Player target){
		if(hits.get(target) != null){
			hits.remove(target);
		}
		if(killstreak.get(target) != null){
			killstreak.remove(target);
		}
	}
	
	private void manageKillStreakMessage(Player killer, int kills) {
		switch(kills){
		case 5:
		case 10:
		case 15:
		case 20:
		case 25:
		case 50:
			for(Player all : Bukkit.getOnlinePlayers()){
				all.sendMessage((kills == 50 ? Language.translate(all,"KILLSTREAK_HACKERMESSAGE") : Language.translate(all,"KILLSTREAK_MESSAGE", KillStreakColor.getPrefix(kills) + Integer.toString(kills))).replace("{1}",killer.getName()));
			}
			break;
		}
		
	}

	public String getSuicideMessage(Player target) {
		String[] suicidemessages = Language.translateArray(target,"SUICIDE_MESSAGES");
		return suicidemessages[r.nextInt(suicidemessages.length-1)].replace("%p", target.getName());
	}
	
	public String[] getKillMessage(Player target, Player killer) {
		String[] killmessages_target = Language.translateArray(target,"KILL_MESSAGES");
		String[] killmessages_killer = Language.translateArray(killer,"KILL_MESSAGES");
		int random = r.nextInt(killmessages_target.length-1);
		return new String[]{killmessages_target[random].replace("%p", target.getName()).replace("%k", killer.getName()),
						    killmessages_killer[random].replace("%p", target.getName()).replace("%k", killer.getName())};
	}

	public Hologram getHologram(Player killer, Player target){

		Location location = target.getLocation();
		location.setY(killer.getLocation().getY() + 1);
		String[] message = Language.translateArray(killer, "HOLOGRAM_KILLMESSAGES");
		List<String> lines = new ArrayList<>();

		for(String s : message){
			lines.add(s.replace("#TARGET#", target.getName()).replace("#XP#", "1"));
		}

		return new Hologram(KnockOut.getInstance(),location, lines);
	}

	public enum KillStreakColor{
		FIVE(5,"§3§l"),
		TEN(10,"§c§l"),
		FIFTEEN(15,"§4§l"),
		TWENTY(20,"§6§l"),
		TWENTY_FIVE(25,"§d§l");

		private int kills;
		private String prefix;

		KillStreakColor(int kills, String prefix){
			this.kills = kills;
			this.prefix = prefix;
		}

		public static String getPrefix(int kills){
			for(KillStreakColor ksc : KillStreakColor.values()){
				if(ksc.kills == kills){
					return ksc.prefix;
				}
			}
			return "§4§lERROR";
		}
	}
	

}
