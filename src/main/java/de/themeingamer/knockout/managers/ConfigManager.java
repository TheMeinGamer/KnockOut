package de.themeingamer.knockout.managers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class ConfigManager {

	private HashMap<String, String> data;
	private ArrayList<String> strings;
	private File f;
	private YamlConfiguration cfg;

	public ConfigManager(File f) {
		this.data = new HashMap<>();
		this.strings = new ArrayList<>();
		this.f = f;
		this.cfg = YamlConfiguration.loadConfiguration(f);
		loadDefaults();
		loadData();
	}

	public String getData(String message) {
		return data.get(message);
	}

	public void reload() {
		loadDefaults();
		loadData();
	}

	private void loadDefaults() {
		cfg.options().copyDefaults(true);

		addDefault("prefix", "&7[&bKnockOut&7] &r");
		addDefault("teamsize", 2);
		
		try {cfg.save(f);} catch (IOException e) {}

	}

	private void loadData() {
		for (String s : strings) {
			data.put(s, ChatColor.translateAlternateColorCodes('&', cfg.getString(s)));
		}
		strings.clear();
	}

	private void addDefault(String key, Object value) {
		cfg.addDefault(key, value);
		strings.add(key);
	}

	public File getFile() {
		return f;
	}

	public YamlConfiguration getYamlConfiguration() {
		return cfg;
	}

}
