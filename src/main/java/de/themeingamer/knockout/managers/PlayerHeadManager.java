package de.themeingamer.knockout.managers;

import de.themeingamer.knockout.itemfactory.SkullFactory;
import de.themeingamer.knockout.language.Language;
import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Creator: TheMeinGamer
 * Date: 26.03.17
 */
public class PlayerHeadManager {

    private ItemStack[] heads;

    public PlayerHeadManager(){
        this.heads = new ItemStack[8];
        ResultSet rs = KnockOut.getInstance().getMySQL().query("SELECT * FROM knockoutheads ORDER BY level");
        int i = 0;
        try {
            while (rs.next()){
                String name = getName(rs.getString("uuid"));
                heads[i] = new SkullFactory().setSkullType(SkullFactory.SkullType.PLAYER).setSkullOwner(name).addLore("§6" + name).build();
                 i++;
            }
        } catch (SQLException e) {
            KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(),"PlayerHeadManager","",e);
        }
    }

    public void checkHead(Player p){
        ItemStack helmet = p.getInventory().getHelmet();

        if(helmet != null && helmet.getType().equals(Material.SKULL)){
            SkullMeta meta = (SkullMeta) helmet.getItemMeta();
            String owner = meta.getOwner();
            KnockOut.getInstance().getMySQL().queryAsync("SELECT * FROM knockoutstats WHERE name='" + owner + "';", (ResultSet rs) ->{
                try {
                    if (rs.next()){
                        KnockOut.getInstance().getMySQL().queryAsync("SELECT * FROM knockoutheads WHERE uuid='" + rs.getString("uuid") + "';",(ResultSet rs1) ->{
                            try {
                                if(!rs1.next()){
                                    resetHead(p);
                                }
                            } catch (SQLException e) {
                                KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(),"PlayerHeadManager.checkHead","" + p,e);
                            }
                        });
                    }else{
                        resetHead(p);
                    }
                } catch (SQLException e) {
                    KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(),"PlayerHeadManager.checkHead","" + p,e);
                }
            });
        }
    }

    private void resetHead(Player p){
        p.getInventory().setHelmet(null);
        p.sendMessage(Language.translate(p,"OLD_HEAD"));
    }

    private String getName(String uuid) throws SQLException {
        ResultSet rs = KnockOut.getInstance().getMySQL().query("SELECT name FROM knockoutstats WHERE uuid='" + uuid + "';");
        if(rs.next()){
            return rs.getString("name");
        }else{
            return "ERROR";
        }
    }

    public ItemStack getHead(int i) {
        return heads[i];
    }
}
