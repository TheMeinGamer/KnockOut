package de.themeingamer.knockout.commands;

import de.themeingamer.knockout.language.Language;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.themeingamer.knockout.main.KnockOut;
import de.themeingamer.knockout.map.Map;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class Command_forcemap implements CommandExecutor{
	

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender.hasPermission("knockout.forcemap")){
			if(args.length == 1){
				Map m = KnockOut.getInstance().getMapManager().getMapByMapName(args[0]);
				if(m != null){
					Map forcemap = KnockOut.getInstance().getMapManager().getMapVote().getForcemap(); 
					if(forcemap == null){
						Map currentmap = KnockOut.getInstance().getMapManager().getCurrentMap();
						if(!m.getMapname().equals(currentmap.getMapname())){
							KnockOut.getInstance().getMapManager().getMapVote().forcemap(m);
							sender.sendMessage(Language.translate(sender,"COMMAND_FORCEMAP_SUCCESS", m.getMapname()));
						}else{
							sender.sendMessage(Language.translate(sender,"COMMAND_FORCEMAP_PLAYING", m.getMapname()));
						}
					}else{
						sender.sendMessage(Language.translate(sender,"COMMAND_FORCEMAP_ALREADYSET", forcemap.getMapname()));
					}
				}else{
					sender.sendMessage(Language.translate(sender,"COMMAND_FORCEMAP_MAPNOTEXISTS", args[0]));
				}
			}else{
				sender.sendMessage(Language.translate(sender,"COMMAND_FORCEMAP_SYNTAXERROR"));
			}
		}else{
			sender.sendMessage(Language.translate(sender,"COMMAND_NOPERM"));
		}
		return true;
	}
	
}
