package de.themeingamer.knockout.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.themeingamer.knockout.main.KnockOut;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class Command_map implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player) || (!sender.hasPermission("knockout.map"))) {
			return true;
		}

		Player p = (Player) sender;
		switch (args.length) {
		case 2:
			switch (args[1].toLowerCase()) {
			case "create":
				p.sendMessage(KnockOut.getInstance().getMapManager().getMapCreator().create(args[0]));
				break;
			case "finish":
				p.sendMessage(KnockOut.getInstance().getMapManager().getMapCreator().finish(args[0]));
				break;
			case "setspawn":
				p.sendMessage(
						KnockOut.getInstance().getMapManager().getMapCreator().setSpawn(args[0], p.getLocation()));
				break;
			case "sethologramspawn":
				p.sendMessage(
						KnockOut.getInstance().getMapManager().getMapCreator().setHologram(args[0], p.getLocation()));
				break;
			case "setmapicon":
				p.sendMessage(
						KnockOut.getInstance().getMapManager().getMapCreator().setMapIcon(args[0], p.getItemInHand()));
				break;
			default:
				sendHelp(p);
				break;
			}
			break;
		case 3:
			switch (args[1].toLowerCase()) {
			case "setauthors":
				p.sendMessage(KnockOut.getInstance().getMapManager().getMapCreator().setAuthors(args[0], args[2]));
				break;
			case "setnopvpheight":
				p.sendMessage(KnockOut.getInstance().getMapManager().getMapCreator().setNoPvPHeight(args[0],
						Integer.valueOf(args[2])));
				break;
			case "setdeathheight":
				p.sendMessage(KnockOut.getInstance().getMapManager().getMapCreator().setDeathHeight(args[0],
						Integer.valueOf(args[2])));
				break;
			case "setspawnradius":
				p.sendMessage(KnockOut.getInstance().getMapManager().getMapCreator().setSpawnRadius(args[0],
						Integer.valueOf(args[2])));
				break;
			default:
				sendHelp(p);
				break;
			}
			break;
		default:
			sendHelp(p);
			break;
		}
		return true;

	}

	private void sendHelp(Player p) {
		p.sendMessage("§7---- " + KnockOut.getInstance().getPrefix() + "§7----\n");
		p.sendMessage("§b/map §7 - Listet alle Befehle auf");
		p.sendMessage("§b/map <Map> create §7- Erstelt die Map");
		p.sendMessage("§b/map <Map> finish §7- Beendet die Map");
		p.sendMessage("§b/map <Map> setSpawn §7- Setzt den Spawn der Map (Spielerlocation)");
		p.sendMessage("§b/map <Map> setHologramSpawn §7- Setzt den HologramSpawn der Map (Spielerlocation)");
		p.sendMessage("§b/map <Map> setMapIcon §7- Setzt das MapIcon der Map (Item in der Hand)");
		p.sendMessage("§b/map <Map> setAuthors <Authors> §7- Setzt die Autoren der Map (Mit ':'  getrennt)");
		p.sendMessage("§b/map <Map> setNoPvPHeight <Höhe> §7- Setzt die NoPvPHeight der Map");
		p.sendMessage("§b/map <Map> setDeathHeight <Höhe> §7- Setzt die DeathHeight der Map");
		p.sendMessage("§b/map <Map> setSpawnRadius <Radius> §7- Setzt den SpawnRadius der Map");
		p.sendMessage("\n§7---- " + KnockOut.getInstance().getPrefix() + "§7----");

	}

}
