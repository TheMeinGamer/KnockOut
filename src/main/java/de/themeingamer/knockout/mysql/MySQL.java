package de.themeingamer.knockout.mysql;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class MySQL {

    private ExecutorService executor = Executors.newCachedThreadPool();

    private Plugin plugin;
    private String host, user, password, database;
    private int port;

    private Connection conn;

    public MySQL(Plugin plugin, String host, String user, String password, String database, int port) throws SQLException, ClassNotFoundException {
        this.plugin = plugin;
        this.host = host;
        this.user = user;
        this.password = password;
        this.database = database;
        this.port = port;
        this.openConnection();
    }
    
    public MySQL(Plugin plugin, File f) throws ClassNotFoundException, SQLException{
    	this.plugin = plugin;
    	YamlConfiguration cfg = YamlConfiguration.loadConfiguration(f);
    	cfg.options().copyDefaults(true);
    	cfg.addDefault("MySQL.Host", "HOST");
    	cfg.addDefault("MySQL.User", "USER");
    	cfg.addDefault("MySQL.Password", "PASSWORD");
    	cfg.addDefault("MySQL.Database", "DATABASE");
    	cfg.addDefault("MySQL.Port", 3306);
    	try { cfg.save(f); } catch (IOException e) {}
    	this.host = cfg.getString("MySQL.Host");
    	this.user = cfg.getString("MySQL.User");
    	this.password = cfg.getString("MySQL.Password");
    	this.database = cfg.getString("MySQL.Database");
    	this.port = cfg.getInt("MySQL.Port");
    	this.openConnection();
    	
    }

    public void queryUpdateAsync(String statemant) {
        executor.execute(() -> {
            queryUpdate(statemant);
        });
    }

    public void queryAsync(String statemant, Consumer<ResultSet> consumer) {
        executor.execute(() -> {
            ResultSet rs = query(statemant);
            Bukkit.getScheduler().runTask(plugin, () -> consumer.accept(rs));
        });
    }

    public void queryUpdate(String statemant) {
        checkConnection();
        PreparedStatement stm = prepare(statemant);
        try {
            stm.executeUpdate();
        } catch (SQLException e) {
        	throwException("[MySQL]","MySQL.queryUpdate","Statemant: " + statemant + " | executeUpdate",e);
        } finally {
            try {
                stm.close();
            } catch (SQLException e) {
            	throwException("[MySQL]","MySQL.queryUpdate","Statemant: " + statemant + " | close",e);
            }
        }
    }

    public ResultSet query(String statemant) {
        checkConnection();
        try {
            return prepare(statemant).executeQuery();
        } catch (SQLException e) {
        	throwException("[MySQL]","MySQL.query","Statemant: " + statemant,e);
        }
        return null;
    }

    public PreparedStatement prepare(String query) {
        try {
            return getConnection().prepareStatement(query);
        } catch (SQLException e) {
        	throwException("[MySQL]","MySQL.prepare","Query: " + query,e);
        }
        return null;
    }

    public Connection getConnection() {
        checkConnection();
        return this.conn;
    }

    private void checkConnection() {
        try {
            if (this.conn == null || this.conn.isValid(10) || this.conn.isClosed()) {
                openConnection();
            }
        } catch (ClassNotFoundException | SQLException e) {
        	throwException("[MySQL]","MySQL.checkConnection","-",e);
        }
    }

    public Connection openConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        return this.conn = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database, this.user, this.password);
    }

    public void closeConnection() {
        try {
            this.conn.close();
        } catch (SQLException e) {
        	throwException("[MySQL]","MySQL.queryUpdate","-",e);
        } finally {
            this.conn = null;
        }
    }
    public void throwException(String prefix, String origin, String message, Exception e){
    	Bukkit.getConsoleSender().sendMessage(prefix + "§bMySQL §7| §4Fehler §7--> Klasse: " + origin + " | " + message + " | Fehler: " + e.toString());
    }

	public String getHost() {
		return host;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getDatabase() {
		return database;
	}

	public int getPort() {
		return port;
	}
    
    
}