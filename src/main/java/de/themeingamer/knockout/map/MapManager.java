package de.themeingamer.knockout.map;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;

import de.themeingamer.knockout.main.KnockOut;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class MapManager {
	
	private MapCreator mapcreator;
	private MapRotation maprotation;
	private MapVote mapvote;
	
	private ArrayList<Map> maps;
	
	public MapManager() {
		this.maps = new ArrayList<>();
		this.mapcreator = new MapCreator();
		this.maprotation = new MapRotation();
	}

	
	public void addMap(String name){
		KnockOut.getInstance().getMySQL().queryAsync("SELECT * FROM knockoutmaps WHERE mapname='" + name + "';", (ResultSet rs) -> {
			try {
				if(rs.next()){
					maps.add(new Map(rs.getString("mapname"),
						rs.getString("mapauthors"),
						rs.getString("spawnlocation"),
						rs.getString("hologramlocation"),
						rs.getInt("nopvpheight"),
						rs.getInt("deathheight"),
						rs.getInt("spawnradius"),
						rs.getString("mapicon")));
				}
			} catch (SQLException e) {
				KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(),"MapManager.addMap","Name: " + name,e);
			}
		});
	}
	
	public void addMap(Map m){
		maps.add(m);
	}

	public Map getMapByMapName(String name){
		for(Map m :  maps){
			if(m.getMapname().equalsIgnoreCase(name)){
				return m;
			}
		}
		return null;
	}
	
	public Map getMapByMapIcon(ItemStack item){
		for(Map m :  maps){
			if(m.getIcon().getType().equals(item.getType())){
				return m;
			}
		}
		return null;
	}
	
	
	public ArrayList<Map> getMaps() {
		return maps;
	}
	
	public Map getCurrentMap(){
		return maprotation.getCurrentMap();
	}
	
	public MapCreator getMapCreator() {
		return mapcreator;
	}
	
	public MapVote getMapVote() {
		return mapvote;
	}

	public MapRotation getMapRotation() {
		return maprotation;
	}

	public void setMapVote(MapVote mapvote) {
		this.mapvote = mapvote;
	}
	

	
}
