package de.themeingamer.knockout.map;

import java.util.ArrayList;
import java.util.HashMap;

import de.themeingamer.knockout.language.Language;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.themeingamer.knockout.main.KnockOut;
import de.themeingamer.knockout.itemfactory.ItemFactory;
import de.themeingamer.knockout.itemfactory.DyedFactory;
import de.themeingamer.knockout.itemfactory.DyedFactory.DyeType;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class MapVote {

	private HashMap<Map,Object[]> votes;
	private Map forcemap;

	public MapVote() {
		this.votes = new HashMap<>();
		loadMaps();
	}

	public void loadMaps() {
		votes.clear();
		for(Map m : getMapManager().getMaps()){
			if(!m.getMapname().equals(getMapManager().getCurrentMap().getMapname())){
				votes.put(m, new Object[]{0,new ArrayList<Player>()});
			}
		}
	}
	
	public void forcemap(Map m){
		forcemap = m;
	}
	
	public void addVote(Player p, ItemStack item){
		
		if(forcemap == null){
			Map m = getMapManager().getMapByMapIcon(item);
			Object[] vote = votes.get(m);
			int count = (int) vote[0];
			ArrayList<Player> voted = (ArrayList<Player>) vote[1];
			if(hasVoted(p)){
				p.playSound(p.getLocation(), Sound.WOLF_WHINE, 1, 1);
				p.sendMessage(Language.translate(p,"MAPVOTE_ALREADYVOTED"));
			}else{
				count++;
				voted.add(p);
				votes.remove(m);
				votes.put(m, new Object[]{count,voted});
				p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
				p.sendMessage(Language.translate(p,"MAPVOTE_SUCESS", m.getMapname()));
			}
		}else{
			p.playSound(p.getLocation(), Sound.WOLF_WHINE, 1, 1);
			p.sendMessage(Language.translate(p,"MAPVOTE_MAPALREADYSET"));
		}
		
		p.closeInventory();
	}
	
	public void openInventory(Player p){
		
		Inventory inv;
		int[] slots = getSlots(votes.keySet().size());
		ItemStack pane = new DyedFactory(DyeType.GLASS_PANE).setColor(DyeColor.GRAY).setDisplayName(" ").build();
		
		if(forcemap == null){
			if(hasVoted(p)){
				inv = Bukkit.createInventory(null, 9, Language.translate(p,"INVENTORY_ALREADYVOTED"));
			} else {
				inv = Bukkit.createInventory(null, 9, Language.translate(p,"INVENTORY_VOTE"));
			}
		}else{
			inv = Bukkit.createInventory(null, 9, Language.translate(p,"INVENTORY_VOTE_MAPSET"));
		}
		
		for(int i = 0; i < 9; i++){
			inv.setItem(i,pane);
		}
		
		int i = 0;
		for(Map m : votes.keySet()){
			inv.setItem(slots[i],new ItemFactory(m.getIcon().getType())
					.setDisplayName((forcemap == null ? "§a§l" : m.getMapname().equals(forcemap.getMapname()) ? "§a§l" : "§7§m") + m.getMapname())
					.setAmount(forcemap == null ? (Integer)votes.get(m)[0] : 1).build());
			i++;
		}
		
		p.openInventory(inv);
	}
	
	
	
	public boolean hasVoted(Player p){
		for(Map m : votes.keySet()){
			ArrayList<Player> voted = (ArrayList<Player>) votes.get(m)[1];
			for(Player all : voted){
				if(all.getUniqueId().equals(p.getUniqueId())){
					return true;
				}
			}
		}
		return false;
	}
	
	public Map getWinMap(){
		if(forcemap != null){
			Map returnmap = forcemap;
			forcemap = null;
			return returnmap;
		}
		Map parammap = votes.keySet().iterator().next();
		for(Map m : votes.keySet()){
			int parammapvotes = (Integer) votes.get(parammap)[0];
			int mapvotes = (Integer) votes.get(m)[0];
			if(mapvotes > parammapvotes){
				parammap = m;
			}
		}
		
		return parammap;
	}
	
	public Map getForcemap() {
		return forcemap;
	}
	
	private int[] getSlots(int count){
		
		int[] slots = new int[count];
		
		switch(count){
		case 1:
			slots[0] = 4;
			break;
		case 2:
			slots[0] = 3;
			slots[1] = 5;
			break;
		case 3:
			slots[0] = 2;
			slots[1] = 4;
			slots[2] = 6;
			break;
		case 4:
			slots[0] = 1;
			slots[1] = 3;
			slots[2] = 5;
			slots[3] = 7;
			break;
		case 5:
			slots[0] = 0;
			slots[1] = 2;
			slots[2] = 4;
			slots[3] = 6;
			slots[4] = 8;
			break;
		case 6:
			slots[0] = 1;
			slots[1] = 2;
			slots[2] = 3;
			slots[3] = 5;
			slots[4] = 6;
			slots[5] = 7;
			break;
		case 7:
			slots[0] = 1;
			slots[1] = 2;
			slots[2] = 3;
			slots[3] = 4;
			slots[4] = 5;
			slots[5] = 6;
			slots[6] = 7;
			break;
		case 8:
			slots[0] = 0;
			slots[1] = 1;
			slots[2] = 2;
			slots[3] = 3;
			slots[4] = 5;
			slots[5] = 6;
			slots[6] = 7;
			slots[7] = 8;
			break;
		case 9:
			slots[0] = 0;
			slots[1] = 1;
			slots[2] = 2;
			slots[3] = 3;
			slots[4] = 4;
			slots[5] = 5;
			slots[6] = 6;
			slots[7] = 7;
			slots[8] = 8;
			break;
		}

		return slots;
	}
	
	
	
	private MapManager getMapManager(){
		return KnockOut.getInstance().getMapManager();
	}
}
