 package de.themeingamer.knockout.map;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import de.themeingamer.knockout.main.KnockOut;
import de.themeingamer.knockout.utils.LocationUtil;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class MapCreator {
	
	public String create(String name) {
		if(mapExists(name)) {
			return ResultMessages.MAPALREADYEXISTS.toString().replace("%MAP%", name);
		}else{
			KnockOut.getInstance().getMySQL().queryUpdateAsync("INSERT INTO knockoutmaps(mapname,state) VALUES ('" + name + "','0');");
			return ResultMessages.MAPSUCCESSFULCREATED.toString().replace("%MAP%", name);
		}
	}
	
	public String setAuthors(String name, String authors) {
		if(mapExists(name)) {
			KnockOut.getInstance().getMySQL().queryUpdateAsync("UPDATE knockoutmaps SET mapauthors='" + authors + "' WHERE mapname='" + name + "';");
			return ResultMessages.MAPAUTHORSSUCCESSFULSET.toString().replace("%MAP%", name).replace("%AUTHORS%", authors.replace(":", ","));
		}else{
			return ResultMessages.MAPNOTEXISTS.toString().replace("%MAP%", name);
		}
	}
	
	public String setSpawn(String name, Location loc){
		if(mapExists(name)) {
			KnockOut.getInstance().getMySQL().queryUpdateAsync("UPDATE knockoutmaps SET spawnlocation='" + LocationUtil.fromLocation(loc) + "' WHERE mapname='" + name + "';");
			return ResultMessages.MAPSPAWNLOCATIONSUCCESSFULSET.toString().replace("%MAP%", name);
		}else{
			return ResultMessages.MAPNOTEXISTS.toString().replace("%MAP%", name);
		}
	}

	public String setHologram(String name, Location loc){
		if(mapExists(name)) {
			KnockOut.getInstance().getMySQL().queryUpdateAsync("UPDATE knockoutmaps SET hologramlocation='" + LocationUtil.fromLocation(loc) + "' WHERE mapname='" + name + "';");
			return ResultMessages.MAPHOLOGRAMLOCATIONSUCCESSFULSET.toString().replace("%MAP%", name);
		}else{
			return ResultMessages.MAPNOTEXISTS.toString().replace("%MAP%", name);
		}
	}
	
	public String setNoPvPHeight(String name, int height) {
		if(mapExists(name)) {
			KnockOut.getInstance().getMySQL().queryUpdateAsync("UPDATE knockoutmaps SET nopvpheight='" + height + "' WHERE mapname='" + name + "';");
			return ResultMessages.MAPNOPVPHEIGHTSUCCESSFULSET.toString().replace("%MAP%", name).replace("%HEIGHT%", height + "");
		}else{
			return ResultMessages.MAPNOTEXISTS.toString().replace("%MAP%", name);
		}
	}
	
	public String setDeathHeight(String name, int height) {
		if(mapExists(name)) {
			KnockOut.getInstance().getMySQL().queryUpdateAsync("UPDATE knockoutmaps SET deathheight='" + height + "' WHERE mapname='" + name + "';");
			return ResultMessages.MAPDEATHHEIGHTSUCCESSFULSET.toString().replace("%MAP%", name).replace("%HEIGHT%", height + "");
		}else{
			return ResultMessages.MAPNOTEXISTS.toString().replace("%MAP%", name);
		}
	}
	
	public String setSpawnRadius(String name, int radius) {
		if(mapExists(name)) {
			KnockOut.getInstance().getMySQL().queryUpdateAsync("UPDATE knockoutmaps SET spawnradius='" + radius + "' WHERE mapname='" + name + "';");
			return ResultMessages.MAPSPAWNRADIUSSUCCESSFULSET.toString().replace("%MAP%", name).replace("%RADIUS%", radius + "");
		}else{
			return ResultMessages.MAPNOTEXISTS.toString().replace("%MAP%", name);
		}
	}
	
	public String setMapIcon(String name, ItemStack item){
		if(mapExists(name)) {
			if(item == null){
				return ResultMessages.NOITEMINHAND.toString();
			}else{
				KnockOut.getInstance().getMySQL().queryUpdateAsync("UPDATE knockoutmaps SET mapicon='" + item.getType().toString() + "' WHERE mapname='" + name + "';");
				return ResultMessages.MAPICONSUCCESSFULSET.toString().replace("%MAP%", name).replace("%TYPE%", item.getType().toString() + "");
			}
		}else{
			return ResultMessages.MAPNOTEXISTS.toString().replace("%MAP%", name);
		}
	}
	
	public String finish(String name){
		if(mapExists(name)){
			ResultSet rs = KnockOut.getInstance().getMySQL().query("SELECT * FROM knockoutmaps WHERE mapname='" + name + "';");
			String returnstring = "";
			try {
				while(rs.next()){
				returnstring = valueExists(rs,"MapName") + "\n" + 
									  valueExists(rs,"MapAuthors") + "\n" + 
									  valueExists(rs,"SpawnLocation") + "\n" +
						              valueExists(rs,"HologramLocation") + "\n" +
						              valueExists(rs,"NoPvPHeight") + "\n" +
									  valueExists(rs,"DeathHeight") + "\n" +
									  valueExists(rs,"SpawnRadius") + "\n" + 
									  valueExists(rs,"MapIcon");
				}
			} catch (SQLException e) {
				KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(),"MapCreator.finish","Name: " + name,e);
			}
			if(returnstring.contains("✘")){
				return ResultMessages.MAPVALUESMISSING.toString().replace("%MAP%", name) + "\n" + returnstring;
			}else{
				KnockOut.getInstance().getMySQL().queryUpdateAsync("UPDATE knockoutmaps SET state='1' WHERE mapname='" + name + "';");
				KnockOut.getInstance().getMapManager().addMap(name);
				return ResultMessages.MAPSUCCESSFULFINISHED.toString().replace("%MAP%", name);
			}
		}else{
			return ResultMessages.MAPNOTEXISTS.toString().replace("%MAP%", name);
		}
	}
	
	private String valueExists(ResultSet rs,String key){
		try {
				if(rs.getString(key) == null){
					return ResultMessages.MAPFINISHLIST.toString().replace("%NAME%", key).replace("%STATE%", "§c✘");
				}else{
					return ResultMessages.MAPFINISHLIST.toString().replace("%NAME%", key).replace("%STATE%", "§a✔");
				}
				
			
		} catch (SQLException e) {
			KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(),"MapCreator.valueExists","Key: " + key,e);
		}	
		return "ERROR";
	}
	
	private boolean mapExists(String name){
		ResultSet rs = KnockOut.getInstance().getMySQL().query("SELECT mapname FROM knockoutmaps WHERE mapname='" + name + "';");
		try {
			boolean exist = rs.next();
			rs.close();
			return exist;
		} catch (SQLException e) {
			KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(),"MapCreator.mapExists","Name: " + name,e);
		}
		return true;
	}
	
	public enum ResultMessages{
		MAPSUCCESSFULCREATED("§7Die Map §b%MAP% §7wurde erstellt"),
		MAPALREADYEXISTS("§cDie Map §b%MAP% §cwurde bereits erstellt"),
		MAPAUTHORSSUCCESSFULSET("§7Die Authoren der Map §b%MAP% wurden auf §b%AUTHORS%§7 gesetzt"),
		MAPSPAWNLOCATIONSUCCESSFULSET("§7Die §bSpawnLocation §7der Map §b%MAP% §7wurde gesetzt"),
		MAPHOLOGRAMLOCATIONSUCCESSFULSET("§7Die §bHologramLocation §7der Map §b%MAP% §7wurde gesetzt"),
		MAPNOPVPHEIGHTSUCCESSFULSET("§7Die §bNoPvPHeight §7von der Map §b%MAP%§7 wurde auf §b%HEIGHT%§7 gesetzt"),
		MAPDEATHHEIGHTSUCCESSFULSET("§7Die §bDeathHeight §7von der Map §b%MAP%§7 wurde auf §b%HEIGHT%§7 gesetzt"),
		MAPSPAWNRADIUSSUCCESSFULSET("§7Der §bSpawnRadius §7von der Map §b%MAP%§7 wurde auf §b%RADIUS%§7 gesetzt"),
		MAPICONSUCCESSFULSET("§7Das §bMapIcon §7von der Map §b%MAP%§7 wurde auf §b%TYPE%§7 gesetzt"),
		MAPFINISHLIST("§7- %NAME%§8: %STATE%"),
		MAPVALUESMISSING("§cDie Map §b%MAP% §ckonnte nicht fertiggestellt werden§8:"),
		MAPSUCCESSFULFINISHED("§7Die Map §b%MAP% §7wurde erfolgreich fertiggestellt!"),
		NOITEMINHAND("§cDu must ein Item in deiner Hand halten"),
		MAPNOTEXISTS("§cDie Map §b%Map% §cexestiert nicht");
		
		private String text;
		
		ResultMessages(String text){
			this.text = text;
		}
		
		@Override
		public String toString() {
			return KnockOut.getInstance().getPrefix() + this.text;
		}
	}
}
