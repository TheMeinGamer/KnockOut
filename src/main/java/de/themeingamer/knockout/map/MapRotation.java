package de.themeingamer.knockout.map;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import de.themeingamer.knockout.account.Account;
import de.themeingamer.knockout.language.Language;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class MapRotation {

	private Map currentmap;
	private int min;
	private int current;
	private int teamsize;
	private List<Object[]> data;

	public MapRotation() {
		min = 15;
		current = 900;
		loadMaps();
	}

	private void loadMaps() {
		teamsize = Integer.parseInt(KnockOut.getInstance().getConfigManager().getData("teamsize"));
		KnockOut.getInstance().getMySQL().queryAsync("SELECT * FROM knockoutmaps WHERE state='1';", (ResultSet rs) -> {
			try {
				while (rs.next()) {
					getMapManager().addMap(new Map(rs.getString("mapname"),
							rs.getString("mapauthors"), rs.getString("spawnlocation"), rs.getString("hologramlocation"),
							rs.getInt("nopvpheight"), rs.getInt("deathheight"),
							rs.getInt("spawnradius"), rs.getString("mapicon")));
				}
				currentmap = getMapManager().getMaps().get(new Random().nextInt(getMapManager().getMaps().size()));
				data = KnockOut.getInstance().getAccountManager().getTop10List();
				KnockOut.getInstance().getMapManager().setMapVote(new MapVote());
				loadActionBar();
				loadRotation();
			} catch (SQLException e) {
				KnockOut.getInstance().getMySQL().throwException(KnockOut.getInstance().getPrefix(),
						"MapRotation.loadMaps", "-", e);
			}
		});

	}

	private void loadRotation() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(KnockOut.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				current--;
				if(current <= 0){
					current = min * 60 + 1;
					currentmap = getMapManager().getMapVote().getWinMap();
					data = KnockOut.getInstance().getAccountManager().getTop10List();
					getMapManager().getMapVote().loadMaps();
					String author = generateAuthorInfo();
					for(Player all : Bukkit.getOnlinePlayers()){
						Account acc = KnockOut.getInstance().getAccountManager().getAccount(all.getUniqueId().toString());
						all.teleport(currentmap.getPlayerSpawn());
						acc.getHologram().hidePlayer(all);
						acc.setHologram(data);
						acc.getHologram().showPlayer(all);
						acc.getAccountInventory().setLobbyInventory();
						acc.update();
						all.sendMessage(Language.translate(all,"MAPROTATION_MAPCHANGE"));
						all.sendMessage(Language.translate(all,"MAPROTATION_MAPINFO", currentmap.getMapname() ,author));
						String[] titles = Language.translate(all,"MAPROTATION_TITLES").split("#SPLIT#");
						KnockOut.getInstance().getTitleManager().sendTitles(all,titles[0].replace("%MAP%",currentmap.getMapname()), titles[1].replace("%AUTHOR%",author), 5, 100, 25);
					}
				}else{
					if(current == 900 || current == 600 || current == 300 || current == 240 || current == 180|| current == 120 || current == 60){
						for(Player all : Bukkit.getOnlinePlayers()){
							String min[] = Language.translate(all,"MAPROTATION_MAPCHANGE_IN_MIN").split("#SPLIT#");
							all.sendMessage(Language.translate(all,"MAPROTATION_MAPCHANGE_IN", (current/60 + " " +  (current == 60 ? min[0] : min[1]))));
							all.playSound(all.getLocation(), Sound.CLICK, 1F, 1F);	
						}
					}else if(current == 30 || current == 10 || current <= 3){
						for(Player all : Bukkit.getOnlinePlayers()){
							String sec[] = Language.translate(all,"MAPROTATION_MAPCHANGE_IN_SEC").split("#SPLIT#");
							all.sendMessage(Language.translate(all,"MAPROTATION_MAPCHANGE_IN", (current + " " + (current == 1 ? sec[0] : sec[1]))));
							all.playSound(all.getLocation(), Sound.CLICK, 1F, 1F);	
						}
					}	
				}
			}
		},20,20);
		
	}

	@SuppressWarnings("deprecation") // Scheduler
	private void loadActionBar() {
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(KnockOut.getInstance(), () -> {
            for (Player all : Bukkit.getOnlinePlayers()) {
                KnockOut.getInstance().getTitleManager().sendActionBar(all, getTeamSizeMessage(all));
            }
        }, 0, 20);

	}

	public String getTeamSizeMessage(Player p){
		String[] message = Language.translate(p,"MAPROTATION_ACTIONBAR").split("#SPLIT#");
		return teamsize <= 1 ? message[0] : message[1].replace("{0}",Integer.toString(teamsize));
	}
	
	private String generateAuthorInfo(){
		if(currentmap.getAuthors().length == 1){
			return "§e" + currentmap.getAuthors()[0];
		}
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0 ; i < currentmap.getAuthors().length; i++){
			if(i < currentmap.getAuthors().length - 1){
				sb.append("§e" + currentmap.getAuthors()[i] + "§8, ");
			}else{
				sb.append("§e" + currentmap.getAuthors()[i]);
			}
		}
		return sb.toString();
	}

	public Map getCurrentMap() {
		return currentmap;
	}
	
	private MapManager getMapManager(){
		return KnockOut.getInstance().getMapManager();
	}

	public List<Object[]> getData() {
		return data;
	}
}
