package de.themeingamer.knockout.map;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.themeingamer.knockout.itemfactory.ItemFactory;
import de.themeingamer.knockout.main.KnockOut;
import de.themeingamer.knockout.utils.LocationUtil;
import de.themeingamer.knockout.account.Account;

/**
 * Creator: TheMeinGamer
 * Date: 10.03.17
 */

public class Map {
	
	private Random r;
	
	private String mapname;
	private String[] authors;
	private Location spawn, hologramlocation;
	private int pvpheight,deathheight,spawnradius;
	private ItemStack icon;
	
	
	public Map(String mapname, String authors, String spawn,String hologramlocation, int pvpheight, int deathheight, int spawnradius, String icon) {
		this.r = new Random();
		this.mapname = mapname;
		this.authors = authors.split(":");
		this.spawn = LocationUtil.fromString(spawn);
		this.hologramlocation = LocationUtil.fromString(hologramlocation);
		this.pvpheight = pvpheight;
		this.deathheight = deathheight;
		this.spawnradius = spawnradius;
		this.icon = new ItemFactory(Material.valueOf(icon)).build();
	}

	public void death(Player target){

		Player killer = KnockOut.getInstance().getDeathManager().getDamager(target);
		if(killer == null){
			target.sendMessage(KnockOut.getInstance().getPrefix() + KnockOut.getInstance().getDeathManager().getSuicideMessage(target));
			KnockOut.getInstance().getDeathManager().kill(target, null);
		}else{
			Account killeracc = KnockOut.getInstance().getAccountManager().getAccount(killer.getUniqueId().toString());
			killer.playSound(killer.getLocation(),killeracc.getKillsound(),1F,1F);
			String[] message = KnockOut.getInstance().getDeathManager().getKillMessage(target,killer);
			target.sendMessage(KnockOut.getInstance().getPrefix() + message[0]);
			killer.sendMessage(KnockOut.getInstance().getPrefix() + message[1]);
			killeracc.setXp(killeracc.getXp()+1);
			KnockOut.getInstance().getDeathManager().kill(target, killer);
			KnockOut.getInstance().getLevelManager().canLevelUp(killer);
		}

		Account targetacc = KnockOut.getInstance().getAccountManager().getAccount(target.getUniqueId().toString());
		target.teleport(getPlayerSpawn());
		target.playSound(target.getLocation(),targetacc.getDeathsound(),1F,1F);
		targetacc.getAccountInventory().setLobbyInventory();

	}


	
	public String getMapname() {
		return mapname;
	}

	public String[] getAuthors() {
		return authors;
	}

	public Location getSpawn() {
		return spawn;
	}

	public Location getHologramLocation() {
		return hologramlocation;
	}
	
	public Location getPlayerSpawn() {
		Location playerlocation = spawn.clone();
		playerlocation.add(r.nextInt((getSpawnradius()*2)-getSpawnradius()),0,(getSpawnradius()*2)-getSpawnradius());
		return playerlocation;
	}

	public int getPvpheight() {
		return pvpheight;
	}

	public int getDeathheight() {
		return deathheight;
	}

	public int getSpawnradius() {
		return spawnradius;
	}
	
	public ItemStack getIcon() {
		return icon;
	}
	
	
	
}
