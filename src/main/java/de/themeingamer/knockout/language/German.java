package de.themeingamer.knockout.language;

import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.ChatColor;

/**
 * Creator: TheMeinGamer
 * Date: 19.03.17
 */

public class German {
    public static final String COMMAND_NOPERM = KnockOut.getInstance().getPrefix() + "§cKeine Berechtigung für diesen Befehl!";
    public static final String COMMAND_FORCEMAP_SYNTAXERROR = KnockOut.getInstance().getPrefix() + "§cFalsche Nutzung§7! /forcemap <Map>";
    public static final String COMMAND_FORCEMAP_MAPNOTEXISTS = KnockOut.getInstance().getPrefix() + "§cDie Map §6{0} §cexistiert nicht§7!";
    public static final String COMMAND_FORCEMAP_ALREADYSET = KnockOut.getInstance().getPrefix() + "§cEs folgt bereits §6{0}§7!";
    public static final String COMMAND_FORCEMAP_PLAYING = KnockOut.getInstance().getPrefix() + "§cEs wird bereits §6{0}§c gespielt§7!";
    public static final String COMMAND_FORCEMAP_SUCCESS = KnockOut.getInstance().getPrefix() + "§aDie Map §6§l{0} §awurde als nächste festgelegt!";
    public static final String INVENTORY_ALREADYVOTED = "§4Du hast bereits gevotet!";
    public static final String INVENTORY_VOTE = "§6§lVote für eine Map!";
    public static final String INVENTORY_VOTE_MAPSET = "§cEs wurde eine Map festgelegt";
    public static final String INVENTORY_REWARDS = "§2§lLevelbelohnungen - Seite {0}";
    public static final String INVENTORY_FEATURE = "§a§lFeatureauswahl";
    public static final String INVENTORY_ARMOREDIT = "§aRüstung editieren";
    public static final String INVENTORY_SKULLEDIT = "§aKopf editieren";
    public static final String INVENTORY_SOUNDEDIT = "§aSounds editieren";
    public static final String INVENTORY_SECONDSOUNDEDIT = "§aSound auswählen";
    public static final String INVENTORY_WEAPONEDIT = "§aWaffe editieren";
    public static final String SECONDSOUNDEDIT_CHOOSEAS = "§6{0} §7auswählen als";
    public static final String SECONDSOUNDEDIT_LISTEN = "§6{0} §7anhören";
    public static final String SECONDSOUNDEDIT_KILLSOUND = "§7» §6§lKillSound";
    public static final String SECONDSOUNDEDIT_DEATHSOUND = "§7» §6§lDeathSound";
    public static final String SECONDSOUNDEDIT_LEVELUPSOUND = "§7» §6§lLevelUp-Sound";
    public static final String SETSOUND = KnockOut.getInstance().getPrefix() + "§6{0} §7ausgewählt: §c{1}§7!";
    public static final String SETWEAPON = KnockOut.getInstance().getPrefix() + "§6» Waffe §7ausgewählt: §c{0}§7!";
    public static final String EDIT_INFORMATION = "§9§lInformationen:";
    public static final String EDIT_LEVEL = "§6§lBenötigtes Level: §e{0}";
    public static final String ARMOREDIT_TYPE = "§6§lRüstungstyp: §e{0}";
    public static final String EDIT_LOWLEVEL = "§cDein Level ist noch nicht hoch genug!";
    public static final String ITEM_MAPVOTE = "§c§lMapauswahl";
    public static final String ITEM_FEATURE = "§6§lFeatureauswahl";
    public static final String ITEM_REWARD = "§a§lLevelbelohnungen";
    public static final String ITEM_FURTHER = "Seite vorwärts ⇉";
    public static final String ITEM_BACK = "⇇ Seite zurück";
    public static final String ITEM_ARMOREDIT = "§aRüstung editieren";
    public static final String ITEM_SOUNDEDIT = "§aSounds editieren";
    public static final String ITEM_WEAPONEDIT = "§aWaffe editieren";
    public static final String ITEM_SKULLEDIT = "§aKopf editieren";
    public static final String ITEM_REMOVE_HELMET = "§cHelm zurücksetzen";
    public static final String ITEM_REMOVE_CHESTPLATE = "§cBrustplatte zurücksetzen";
    public static final String ITEM_REMOVE_LEGGINGS = "§cHose zurücksetzen";
    public static final String ITEM_REMOVE_BOOTS = "§cSchuhe zurücksetzen";
    public static final String ITEM_REMOVE_SKULL = "§cKopf zurücksetzen";
    public static final String OLD_HEAD = KnockOut.getInstance().getPrefix() + "Dein Kopf ist nicht mehr aktuell. Bitte wähle einen neuen Kopf.";
    public static final String MAPROTATION_MAPCHANGE = KnockOut.getInstance().getPrefix() + "§6§lMapwechsel!";
    public static final String MAPROTATION_MAPINFO = KnockOut.getInstance().getPrefix() + "§7Gespielt wird nun §a§l{0} §7von §b§l{1}§7!";
    public static final String MAPROTATION_TITLES = "§6§l{0}#SPLIT#§7von {1}";
    public static final String MAPROTATION_ACTIONBAR = "§cKeine Teams#SPLIT#§a{0}er Teams";
    public static final String MAPROTATION_MAPCHANGE_IN = KnockOut.getInstance().getPrefix() + "§7Mapwechsel in §b§l{0}§7!";
    public static final String MAPROTATION_MAPCHANGE_IN_MIN = "Minute#SPLIT#Minuten";
    public static final String MAPROTATION_MAPCHANGE_IN_SEC = "Sekunde#SPLIT#Sekunden";
    public static final String MAPVOTE_ALREADYVOTED = KnockOut.getInstance().getPrefix() + "§cDu hast bereits gevotet!";
    public static final String MAPVOTE_SUCESS = KnockOut.getInstance().getPrefix() + "§7Du hast für die Map §a§l{0} §7gevotet!";
    public static final String MAPVOTE_MAPALREADYSET = KnockOut.getInstance().getPrefix() + "§cEs wurde bereits eine Map festgelegt";
    public static final String[] SUICIDE_MESSAGES = new String[]{"§6%p §ahatte keine Lust mehr zu leben!", "§6%p §awar lebensmüde!",
                                                                 "§6%p §aist nun tot!", "§6%p §asagt dem Leben \"Goodbye\"!",
                                                                 "§6%p §ahat den Weg nicht gesehen!", "§6%p §akann nicht geradeaus laufen!",
                                                                 "§6%p §ahat sich zu weit aus dem Fenster gelehnt!", "§6%p §ahatte einen Lag!"};
    public static final String[] KILL_MESSAGES = new String[]{"§6%k §ahat §6%p §ain die Leere geworfen!", "§6%k §ahat §6%p §aausgeschalten!",
                                                              "§6%k §abrachte §6%p §aunter die Erde!", "§6%k §aerfüllte §6%p §aseinen Todeswunsch!",
                                                              "§6%k §ahat §6%p §aan NoKnockback erinnert!", "§6%k §aschubste §6%p §anach unten!",
                                                              "§6%k §ahat §6%p §azurecht gewiesen!", "§6%k §agab §6%p §aeinen mit!",
                                                              "§6%k §amochte §6%p §anicht!", "§6%k §abrachte §6%p §adas Fliegen bei!"};
    public static final String[] HOLOGRAM_KILLMESSAGES = new String[]{"§7Tötung von #TARGET#", "§a+#XP# XP"};
    public static final String HOLOGRAM_TOP10 = "§1#{0} §3{1} §c{2} Kills §8[§aLevel {3}§8]";
    public static final String KILLSTREAK_MESSAGE = KnockOut.getInstance().getPrefix() + "§6{1} §ahat eine {0}er Killstreak§a!";
    public static final String KILLSTREAK_HACKERMESSAGE = KnockOut.getInstance().getPrefix() + "§6{0} §ahat eine §5§lHacker Killstreak§a!";
    public static final String LEVELUP_MESSAGE = KnockOut.getInstance().getPrefix() + "§2§lDu bist im Level aufgestiegen!";
    public static final String LEVELUP_NEWLEVEL = KnockOut.getInstance().getPrefix() + "§aNeues Level: §e{0}";
    public static final String LEVELUP_NEEDEDKILLS = KnockOut.getInstance().getPrefix() + "§aBenötigte Kills: §e{0} Kills";
    public static final String FEATURE_FEATURETYPE_LEVEL = "§9§lLevelzahl§8: ";
    public static final String FEATURE_FEATURETYPE_ARMOR = "§9§lRüstung§8: ";
    public static final String FEATURE_FEATURETYPE_WEAPON = "§9§lWaffenskin§8: ";
    public static final String FEATURE_FEATURETYPE_SOUND = "§9§lSound§8: ";
    public static final String FEATURE_FEATURETYPE_SKULL = "§9§lKopf§8: ";
    public static final String FEATURE_FEATURELEVEL_BRIGHTGREEN = "§aHellgrün";
    public static final String FEATURE_FEATURELEVEL_GREEN = "§2Grün";
    public static final String FEATURE_FEATURELEVEL_BRIGHTBLUE = "§bHellblau";
    public static final String FEATURE_FEATURELEVEL_BLUE = "§3Blau";
    public static final String FEATURE_FEATURELEVEL_YELLOW = "§eGelb";
    public static final String FEATURE_FEATURELEVEL_ORANGE = "§6Orange";
    public static final String FEATURE_FEATURELEVEL_BRIGHTRED = "§cHellrot";
    public static final String FEATURE_FEATURELEVEL_RED = "§4Rot";
    public static final String FEATURE_FEATUREARMOR_LEATHER_HELMET = "§6Lederhelm";
    public static final String FEATURE_FEATUREARMOR_LEATHER_CHESTPLATE = "§6Lederbrustplatte";
    public static final String FEATURE_FEATUREARMOR_LEATHER_LEGGINGS = "§6Lederhose";
    public static final String FEATURE_FEATUREARMOR_LEATHER_BOOTS = "§6Lederschuhe";
    public static final String FEATURE_FEATUREARMOR_CHAINMAIL_HELMET = "§6Kettenhelm";
    public static final String FEATURE_FEATUREARMOR_CHAINMAIL_CHESTPLATE = "§6Kettenhemd";
    public static final String FEATURE_FEATUREARMOR_CHAINMAIL_LEGGINGS = "§6Kettenhose";
    public static final String FEATURE_FEATUREARMOR_CHAINMAIL_BOOTS = "§6Kettenchuhe";
    public static final String FEATURE_FEATUREARMOR_GOLD_HELMET = "§6Goldhelm";
    public static final String FEATURE_FEATUREARMOR_GOLD_CHESTPLATE = "§6Goldbrustplatte";
    public static final String FEATURE_FEATUREARMOR_GOLD_LEGGINGS = "§6Goldhose";
    public static final String FEATURE_FEATUREARMOR_GOLD_BOOTS = "§6Goldschuhe";
    public static final String FEATURE_FEATUREARMOR_IRON_HELMET = "§6Eisenhelm";
    public static final String FEATURE_FEATUREARMOR_IRON_CHESTPLATE = "§6Eisenbrustplatte";
    public static final String FEATURE_FEATUREARMOR_IRON_LEGGINGS = "§6Eisenhose";
    public static final String FEATURE_FEATUREARMOR_IRON_BOOTS = "§6Eisenschuhe";
    public static final String FEATURE_FEATUREWEAPON_STICK = "§6Stick";
    public static final String FEATURE_FEATUREWEAPON_WOOD_HOE = "§6Holzhacke";
    public static final String FEATURE_FEATUREWEAPON_WOOD_AXE = "§6Holzaxt";
    public static final String FEATURE_FEATUREWEAPON_WOOD_SWORD = "§6Holzschwert";
    public static final String FEATURE_FEATUREWEAPON_STONE_HOE = "§6Steinhacke";
    public static final String FEATURE_FEATUREWEAPON_STONE_AXE = "§6Steinaxt";
    public static final String FEATURE_FEATUREWEAPON_STONE_SWORD = "§6Steinschwert";
    public static final String FEATURE_FEATUREWEAPON_GOLD_HOE = "§6Goldhacke";
    public static final String FEATURE_FEATUREWEAPON_GOLD_AXE = "§6Goldaxt";
    public static final String FEATURE_FEATUREWEAPON_GOLD_SWORD = "§6Goldschwert";
    public static final String FEATURE_FEATUREWEAPON_IRON_HOE = "§6Eisenhacke";
    public static final String FEATURE_FEATUREWEAPON_IRON_AXE = "§6Eisenaxt";
    public static final String FEATURE_FEATUREWEAPON_IRON_SWORD = "§6Eisenschwert";
    public static final String FEATURE_FEATUREWEAPON_DIAMOND_HOE = "§6Diamanthacke";
    public static final String FEATURE_FEATUREWEAPON_DIAMOND_AXE = "§6Diamantaxt";
    public static final String FEATURE_FEATUREWEAPON_DIAMOND_SWORD = "§6Diamantschwert";
}