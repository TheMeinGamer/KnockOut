package de.themeingamer.knockout.language;

import de.themeingamer.knockout.main.KnockOut;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.text.MessageFormat;

/**
 * Creator: TheMeinGamer
 * Date: 19.03.17
 */
public class Language {

    public static String translate( Object p, String path, Object... args) {
        Field field = getField(p,path);
        if ( field != null ) {
            try {
                String translatedString = (String) field.get( null );

                MessageFormat msgFormat = new MessageFormat( translatedString );
                translatedString = msgFormat.format( args );

                return translatedString;

            } catch ( IllegalAccessException e ) {
                e.printStackTrace();
            }
        }

        //Return ErrorString if an Issue accrues & Print in Console
        KnockOut.getInstance().writeInConsole(KnockOut.getInstance().getPrefix() + "§4Fehler beim Übersetzen§7: " + path);
        return "error while translating!";
    }

    public static String[] translateArray( Object p, String path, Object... args) {
        Field field = getField(p,path);
        if ( field != null ) {
            try {
                String[] translatedString = (String[])field.get( null );

                for(int i = 0; i <  translatedString.length; i++){
                    MessageFormat msgFormat = new MessageFormat( translatedString[i] );
                    translatedString[i] = msgFormat.format( args );
                }
                return translatedString;

            } catch ( IllegalAccessException e ) {
                e.printStackTrace();
            }
        }

        //Return ErrorString if an Issue accrues & Print in Console
        KnockOut.getInstance().writeInConsole(KnockOut.getInstance().getPrefix() + "§4Fehler beim Übersetzen§7: " + path);
        return new String[]{"error while translating!"};
    }

    private static Field getField(Object p, String path){
        Field field = null;
        String language = "de";

        if ( p instanceof Player) {
            //language = p.getLocale();
        }
        //Check Language
        switch (language){
            case "en":
                try {
                    field = English.class.getDeclaredField( path );
                } catch ( NoSuchFieldException e ) {
                    e.printStackTrace();
                }
                break;
            default:
                try {
                    field = German.class.getDeclaredField( path );
                } catch ( NoSuchFieldException e ) {
                    e.printStackTrace();
                }
                break;
        }

        //Return translated String if no issues accrue
        return field;
    }
}
